"""
Unit tests for zhtm.sdr
"""
import pytest

from zhtm.sdr import SDR


class TestSDR:
    """
    NOTE: SDR did not always inherit from `set`, so many of these tests may be superfluous.
    However, seeing as the definition of SDR has changed a bit already, I'm keeping them
    here in case the implementation of SDR changes drastically again.
    """

    def test_basic_init(self):
        SDR([])
        SDR([1, 5, 2, 3])

    def test_equality(self):
        a = SDR({1, 2, 3})
        b = SDR({1, 2, 3})
        c = SDR([1, 1, 1, 2, 3])  # show that repeated indices ignored
        d = SDR({1, 2})  # diff active indices

        assert a == a
        assert a == b
        assert a == c
        assert a != d

    and_data = [
        # (self_indices, other_indices, expected_and_indices)
        ({}, {}, {}),
        ({1}, {}, {}),
        ({}, {1}, {}),
        ({1}, {1}, {1}),
        ({1, 2}, {1}, {1}),
        ({1, 2, 3}, {2, 3, 4}, {2, 3}),
        ({1, 2, 3}, {5, 8, 10}, {}),
    ]

    @pytest.mark.parametrize("left,right,expected", and_data)
    def test_and(self, left, right, expected):
        left_sdr = SDR(left)
        right_sdr = SDR(right)

        actual_sdr = left_sdr & right_sdr
        expected_sdr = SDR(expected)

        assert expected_sdr == actual_sdr
        assert isinstance(actual_sdr, SDR)

    or_data = [
        # (self_indices, other_indices, expected_or_indices)
        ({}, {}, {}),
        ({1}, {}, {1}),
        ({}, {1}, {1}),
        ({1}, {1}, {1}),
        ({1, 2}, {1}, {1, 2}),
        ({1, 2, 3}, {2, 3, 4}, {1, 2, 3, 4}),
        ({1, 2, 3}, {5, 8, 10}, {1, 2, 3, 5, 8, 10}),
    ]

    @pytest.mark.parametrize("left,right,expected", or_data)
    def test_or(self, left, right, expected):
        left_sdr = SDR(left)
        right_sdr = SDR(right)

        actual_sdr = left_sdr | right_sdr
        expected_sdr = SDR(expected)

        assert expected_sdr == actual_sdr
        assert isinstance(actual_sdr, SDR)

    difference_data = [
        # (left_indices, right_indices, expected_diff_indices)
        ({}, {}, {}),
        ({}, {1, 2, 3}, {}),
        ({1, 2, 3}, {}, {1, 2, 3}),
        ({1, 2, 3}, {1, 2, 3}, {}),
        ({1, 2, 3}, {1, 2}, {3}),
        ({1, 2, 3}, {3, 4, 5}, {1, 2}),
    ]

    @pytest.mark.parametrize("left,right,expected_diff_indices", difference_data)
    def test_difference(self, left, right, expected_diff_indices):
        left_sdr = SDR(left)
        right_sdr = SDR(right)

        actual_sdr = left_sdr - right_sdr
        expected_sdr = SDR(expected_diff_indices)

        assert expected_sdr == actual_sdr
        assert isinstance(actual_sdr, SDR)

    overlap_data = [
        ([1], [1], 1),
        ([1, 2], [1], 1),
        ([], [1], 0),
        ([], [], 0),
        ([1, 2, 3, 4], [1, 3, 4], 3),
    ]

    @pytest.mark.parametrize("a,b,expected_overlap", overlap_data)
    def test_overlap_expected(self, a, b, expected_overlap):
        a = SDR(a)
        b = SDR(b)
        overlap = a.overlap(b)
        assert overlap == expected_overlap

    match_data = [
        ([1], [1], 1, True),
        ([1], [1], 2, False),
        ([2], [1], 1, False),
        ([1, 2, 3], [1], 1, True),
        ([1, 2, 3], [1], 2, False),
        ([1, 2, 3, 4, 5], [1, 3], 2, True),
    ]

    @pytest.mark.parametrize("a,b,threshold,expected_match", match_data)
    def test_match_expected(self, a, b, threshold, expected_match):
        a = SDR(a)
        b = SDR(b)
        is_match = a.match(b, threshold)
        assert is_match == expected_match

    def test_random_sdr(self):
        num_bits = 200
        num_ones = 40
        for _ in range(100):
            s = SDR.random(num_bits=num_bits, num_ones=num_ones)
            assert len(s) == num_ones
            for index in s:
                assert 0 <= index < num_bits

    bin_string_data = [
        # (input_addresses, num_bits, expected string)
        ([0, 1, 2, 3], 4, "1111"),
        ([0, 2], 4, "1010"),
        ([6, 7, 8, 9], 10, "0000001111"),
        (
            [6, 7, 8, 9],
            4,
            "0000",
        ),  # num_bits too small.  Nonsensical, but expected result
    ]

    @pytest.mark.parametrize("inputs,num_bits,expected_string", bin_string_data)
    def test_as_binary_string(self, inputs, num_bits, expected_string):
        s = SDR(inputs)
        assert s.as_binary_string(num_bits) == expected_string
