import pytest

from zhtm.dendrite_segment import DendriteSegment, Synapse
from zhtm.sdr import SDR


class TestSynapse:
    def test_basic_init(self):
        address = 0
        permanence_value = 0.5
        s = Synapse(address, permanence_value)

        assert address == s.address
        assert permanence_value == s.permanence

    delta_data = [
        # (starting_perm, delta, expected_perm)
        (0.0, 0.5, 0.5),
        (0.0, -0.5, 0.0),
        (0.9, 0.5, 1.0),
    ]

    @pytest.mark.parametrize("start_perm,perm_delta,expected_perm", delta_data)
    def test_permanence_update(self, start_perm, perm_delta, expected_perm):
        s = Synapse(0, start_perm)
        s.permanence += perm_delta
        assert expected_perm == s.permanence

    permanence_data = [
        # (set value, expected_value)
        (0.5, 0.5),
        (0.0, 0.0),
        (1.0, 1.0),
        (-1.0, 0.0),
        (2.0, 1.0),
    ]

    @pytest.mark.parametrize("input_val,expected_val", permanence_data)
    def test_permanence_bounds(self, input_val, expected_val):
        a = Synapse("fake_address", input_val)
        assert a.permanence == expected_val


class TestDendriteSegment:
    def setup_method(self):
        self.d = DendriteSegment()

    def test_update_connected_sdr(self):
        """
        Show that `_update_connected_sdr()` actually updates `self.connected_sdr`.
        :return:
        """
        d = DendriteSegment()
        d.potential_sdr = SDR({0, 1, 2, 3, 4})
        for i in range(5):
            d.potential_synapses[i].permanence = 1.0 if i % 2 == 0 else 0

        d._update_connected_sdr()
        assert d.connected_sdr == {0, 2, 4}

    def test_randomize_potential_synapses(self):
        num_bits_in_input_space = 1000  # arbitrarily chosen
        expected_number_of_potentials = 200  # arbitrary chosen

        d = DendriteSegment()

        # Assert initial conditions to prove they change after function call
        assert d.potential_synapses == {}
        assert d.potential_sdr == SDR({})
        assert d.connected_sdr == SDR({})

        d.randomize_potential_synapses(
            expected_number_of_potentials, num_bits_in_input_space
        )

        # Check number of potential synapses in a couple of places
        assert expected_number_of_potentials == len(d.potential_synapses)
        assert len(d.potential_synapses) == len(d.potential_sdr)

        for i in d.potential_synapses:
            assert 0 <= i < num_bits_in_input_space

        assert d.connected_sdr != {}

    def test_potential_synapse_setter(self):
        d = DendriteSegment()
        potential_sdr = SDR({1, 200, 3000})

        # Assert initial conditions to prove they change after function call
        assert d.potential_synapses == {}
        assert d.potential_sdr == SDR({})
        assert d.connected_sdr == SDR({})

        d.potential_sdr = potential_sdr

        # Check number of potential synapses in a couple of places
        assert potential_sdr == d.potential_sdr
        assert len(d.potential_synapses) == len(d.potential_sdr)

        # Verify contents of d.potential_synapses
        for val in d.potential_synapses.values():
            assert isinstance(val, Synapse)

        # Verify `potential_sdr.indices` translated into `potential_synapses` dict
        assert d.potential_sdr == {s.address for s in d.potential_synapses.values()}

        # Verify that d.connected_sdr updated, aka no longer None
        assert len(d.connected_sdr) <= len(d.potential_synapses)

    def test_process_input(self):
        d = DendriteSegment()
        d.potential_sdr = SDR({1, 3, 5, 7, 9, 11})

        assert d.active_sdr == SDR({})
        assert d.active_connected_sdr == SDR({})

        input_sdr = SDR({-1, 0, 1, 2, 3})
        d.process_input(input_sdr)

        assert isinstance(d.active_sdr, SDR)
        assert isinstance(d.active_connected_sdr, SDR)
        assert d.active_connected_sdr == d.active_sdr & d.connected_sdr

    active_data = [
        # (active_synapses, activation_threshold, is_active)
        ({}, 0, True),  # Corner case; threshold of 0 == always activate?
        ({}, 1, False),
        ({-1}, 1, True),
        ({-1, 1}, 1, True),
        ({-1, 1}, 3, False),
    ]

    @pytest.mark.parametrize("active,threshold,expected_active", active_data)
    def test_is_active(self, active, threshold, expected_active):
        d = DendriteSegment()
        d.active_connected_sdr = SDR(active)

        assert expected_active == d.is_active(threshold)

    match_data = [
        # (input_indices, match_threshold, is_match)
        ({}, 1, False),
        ({1}, 1, True),  # Works for active, unconnected synapse
        ({5}, 1, True),  # Works for active, connected synapse
        ({1, 10}, 1, True),  # One potential overlap, one miss
        ({-2, -1}, 1, False),  # No overlap with input
    ]

    @pytest.mark.parametrize("indices,threshold,is_match", match_data)
    def test_is_matching(self, indices, threshold, is_match):
        d = DendriteSegment()
        d.potential_sdr = SDR({0, 1, 2, 3, 4, 5, 6, 7, 8, 9})
        d.connected_sdr = SDR(
            {5, 6, 7, 8, 9}
        )  # Overwriting the real, randomized values
        assert d.connected_sdr == {5, 6, 7, 8, 9}

        input_sdr = SDR(indices)
        d.process_input(input_sdr)
        assert is_match == d.is_matching(threshold)

    def test_adjust_active_permanences(self):
        self.d.potential_sdr = SDR([1, 2, 3, 4, 5])
        self.d.active_sdr = SDR([1, 2, 3])

        base_permanence = 0.5
        adjustment_amount = 0.1
        for _, synapse in self.d.potential_synapses.items():
            synapse.permanence = base_permanence

        self.d.adjust_active_permanences(adjustment_amount)

        for address, synapse in self.d.potential_synapses.items():
            if address in self.d.active_sdr:
                # Show that all 'active' synapses changed permanence
                assert synapse.permanence == base_permanence + adjustment_amount
            else:
                # Show that all 'inactive' synapses are unchanged
                assert synapse.permanence == base_permanence

    def test_adjust_inactive_permanences(self):
        self.d.potential_sdr = SDR([1, 2, 3, 4, 5])
        self.d.active_sdr = SDR([1, 2, 3])

        base_permanence = 0.5
        adjustment_amount = 0.1
        for _, synapse in self.d.potential_synapses.items():
            synapse.permanence = base_permanence

        self.d.adjust_inactive_permanences(adjustment_amount)

        for address, synapse in self.d.potential_synapses.items():
            if address in self.d.active_sdr:
                # Show that all 'active' synapses are unchanged
                assert synapse.permanence == base_permanence
            else:
                # Show that all 'inactive' synapses changed permanence
                assert synapse.permanence == base_permanence + adjustment_amount

    adapt_data = [
        # (active_indices, prev_active, column_correctly_active, expected_permanences)
        (
            {0, 1},
            {0},
            True,
            [0.6, 0.45],
        ),  # Column correctly active, 1 overlapping w/previous
        (
            {0, 1},
            {0},
            False,
            [0.475, 0.5],
        ),  # Column incorrectly active, 1 overlapping w/previous
        (
            {0, 1},
            {2},
            True,
            [0.45, 0.45],
        ),  # column correctly active, no overlap w/previous
        (
            {0, 1},
            {2},
            False,
            [0.5, 0.5],
        ),  # column incorrectly active, no overlap w/previous
    ]

    @pytest.mark.parametrize("active,previous,col_correct,expected_perms", adapt_data)
    def test_adapt_permanences(self, active, previous, col_correct, expected_perms):
        # TODO: move this (and d.adapt_permanences()) into TMColumn?
        d = DendriteSegment()
        d.permanence_increment = 0.1
        d.permanence_decrement = 0.05
        d.predicted_decrement = 0.025

        d.potential_sdr = SDR({0, 1})
        for k, s in d.potential_synapses.items():
            s.permanence_value = 0.5  # Hard-code to 0.5 to make testing easier
        d.active_sdr = SDR(active)

        column_correctly_active = col_correct
        previous_active_cells = previous
        d.adapt_permanences(column_correctly_active, previous_active_cells)

        for i, v in d.potential_synapses.items():
            assert expected_perms[i] == v.permanence_value

    grow_data = [
        # (existing_indices, winning_indices, num_desired, expected_final_count)
        (set(), set(), 5, 0),  # No error if no winning_indices to choose from
        (set(), {1, 2, 3, 4}, 4, 4),  # Add all
        ({1}, {1, 2, 3, 4}, 4, 4),  # Add 3 new synapses, resulting in 4
        ({1, 2, 3, 4}, {1, 2, 10, 4}, 4, 4),  # Add no new synapses, as all overlap
        ({1}, {1, 2, 3, 4, 5}, 5, 5),  # Add 2 through 5, resulting in 5 total
    ]

    @pytest.mark.parametrize("existing,winning,num_desired,final_count", grow_data)
    def test_grow_new_synapses(self, existing, winning, num_desired, final_count):
        d = DendriteSegment()
        d.potential_sdr = SDR(existing)

        # Add a 'tagged' attribute to existing synapses to make sure they don't get replaced
        for i in existing:
            d.potential_synapses[i].tagged = True

        d.grow_new_synapses(winning, num_desired)

        assert final_count == len(d.potential_sdr) == len(d.potential_synapses)

        for i in existing:
            assert hasattr(d.potential_synapses[i], "tagged")

        for i in d.potential_sdr - existing:
            assert not hasattr(d.potential_synapses[i], "tagged")
            assert (
                d.potential_synapses[i].permanence_value == d.initial_distal_permanence
            )

    def test_grow_new_synapses_before_initialization(self):
        d = DendriteSegment()
        num_new_synapses = 3
        d.grow_new_synapses({1, 2, 3, 4, 5}, num_new_synapses)

    def test_grow_new_synapses_updates_connected_sdr(self):
        d = DendriteSegment()
        d.potential_sdr = SDR({1, 2, 3, 4, 5})

        d.initial_distal_permanence = 1.0  # Every new synapse _should_ be connected
        arbitrary_indices = {10, 20, 30, 40, 50, 60, 70, 80}

        d.grow_new_synapses(arbitrary_indices, 9)
        assert d.connected_sdr == {
            s.address
            for s in d.potential_synapses.values()
            if s.permanence > d.connected_permanence
        }

    def test_grow_new_synapses_without_winners_grown_no_new_synapses(self):
        self.d.randomize_potential_synapses(10, 100)
        pre_count = len(self.d.potential_synapses)
        self.d.grow_new_synapses(SDR(), 100)
        post_count = len(self.d.potential_synapses)
        assert pre_count == post_count
