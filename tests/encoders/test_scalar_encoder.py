import pytest

from zhtm.encoders.scalar_encoder import ScalarEncoder


class TestScalarEncoder:
    data = [
        # (min, max, num_active_bits, num_buckets, input_val, expected_sdr)
        (0, 9, 1, 10, 0, "10000000000"),  # 0-9 is 10 'slots'
        (0, 9, 1, 10, 5, "00000100000"),
        (1, 10, 1, 10, 1, "10000000000"),  # 1-10 is still 10 'slots'
        (1, 10, 1, 10, 10, "00000000001"),
        (1, 10, 1, 10, 5, "00001000000"),
        (1, 10, 1, 10, 0, "10000000000"),  # val < min_val results in min-val pattern
        (1, 10, 1, 10, 11, "00000000001"),  # val > max_val results in max-val pattern
        (1, 10, 2, 10, 0, "110000000000"),
        (1, 10, 2, 10, 10, "000000000011"),
        (1, 10, 3, 10, 1, "1110000000000"),
        (1, 10, 3, 10, 5, "0000111000000"),
        (1, 10, 3, 10, 10, "0000000000111"),
        (1, 10, 5, 20, 1, "1111100000000000000000000"),
        (1, 10, 5, 20, 1.5, "0111110000000000000000000"),
        (1, 10, 5, 20, 2, "0011111000000000000000000"),
        (1, 10, 5, 20, 10, "0000000000000000000011111"),
        (1, 10, 5, 20, 9.66, "0000000000000000000111110"),
        (1, 10, 5, 20, 9.5, "0000000000000000001111100"),
        (1, 10, 5, 20, 9, "0000000000000000011111000"),
    ]

    @pytest.mark.parametrize(
        "min_x, max_x, n_active, n_bucket, input_val, expected", data
    )
    def test_scalar_output(self, min_x, max_x, n_active, n_bucket, input_val, expected):
        s = ScalarEncoder(min_x, max_x, n_active, n_bucket)
        output = s.encode(input_val)
        assert output.as_binary_string(s.num_bits) == expected

    longer_examples = [
        # (min, max, num_active_bits, num_buckets, input_val, expected_zeroes_before_1)
        (-1.0, 1.0, 21, 100, -1.0, 0),
        (-1.0, 1.0, 21, 100, 0, 50),
        (-1.0, 1.0, 21, 100, 1.0, 100),
    ]

    @pytest.mark.parametrize(
        "min_x, max_x, n_active, n_bucket, input_val, expect_left_zero", longer_examples
    )
    def test_larger_examples(
        self, min_x, max_x, n_active, n_bucket, input_val, expect_left_zero
    ):
        s = ScalarEncoder(min_x, max_x, n_active, n_bucket)
        output = s.encode(input_val)

        num_zeros_before_ones = len(
            output.as_binary_string(s.num_bits).split("1", 1)[0]
        )
        assert num_zeros_before_ones == expect_left_zero
