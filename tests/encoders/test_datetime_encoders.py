from datetime import date, datetime

import pytest

from zhtm.encoders.datetime_encoders import (
    DayOfWeekEncoder,
    HourOfDayEncoder,
    MonthEncoder,
    WeekendEncoder,
)


class TestWeekendEncoder:
    data = [
        # (dt, bits_per_category, bit_buffer, expected)
        (date(1900, 1, 1), 10, 0, "11111111110000000000"),  # Monday
        (date(1900, 1, 6), 10, 0, "00000000001111111111"),  # Saturday
        (date(1900, 1, 7), 10, 0, "00000000001111111111"),  # Sunday
        (date(1900, 1, 8), 10, 0, "11111111110000000000"),  # Next Monday
        (datetime(1900, 1, 1), 10, 0, "11111111110000000000"),  # Work with datetime
        (date(1900, 1, 1), 1, 0, "10"),  # Monday
        (date(1900, 1, 6), 1, 0, "01"),  # Saturday
    ]

    @pytest.mark.parametrize("dt,cat_bits,bit_buf,expected", data)
    def test_encode(self, dt, cat_bits, bit_buf, expected):
        e = WeekendEncoder(cat_bits, bit_buf)
        actual = e.encode(dt)
        assert expected == actual.as_binary_string(e.num_bits)


class TestDayOfWeekEncoder:
    data = [
        # (dt, bits_per_category, bit_buffer, expected)
        (date(1900, 1, 1), 3, 0, "111000000000000000000"),
        (date(1900, 1, 1), 3, 2, "111110000000000000011"),
        (date(1900, 1, 3), 3, 0, "000000111000000000000"),
        (date(1900, 1, 7), 3, 0, "000000000000000000111"),
        (date(1900, 1, 7), 3, 2, "110000000000000011111"),
        (datetime(1900, 1, 1), 3, 0, "111000000000000000000"),
    ]

    @pytest.mark.parametrize("dt,cat_bits,bit_buf,expected", data)
    def test_encode(self, dt, cat_bits, bit_buf, expected):
        e = DayOfWeekEncoder(cat_bits, bit_buf)
        actual = e.encode(dt)
        assert expected == actual.as_binary_string(e.num_bits)


class TestMonthEncoder:
    data = [
        # (dt, bits_per_category, bit_buffer, expected)
        (date(1900, 1, 1), 3, 0, "111000000000000000000000000000000000"),
        (date(1900, 1, 1), 3, 2, "111110000000000000000000000000000011"),
        (date(1900, 5, 16), 3, 0, "000000000000111000000000000000000000"),
        (date(1900, 11, 20), 3, 0, "000000000000000000000000000000111000"),
        (date(1900, 11, 20), 3, 6, "111000000000000000000000111111111111"),
    ]

    @pytest.mark.parametrize("dt,cat_bits,bit_buf,expected", data)
    def test_encode(self, dt, cat_bits, bit_buf, expected):
        e = MonthEncoder(cat_bits, bit_buf)
        actual = e.encode(dt)
        assert expected == actual.as_binary_string(e.num_bits)


class TestHourOfDayEncoder:
    dt = datetime
    data = [
        # (dt, bits_per_category, bit_buffer, expected)
        (dt(1900, 1, 1, 0), 1, 0, "100000000000000000000000"),
        (dt(1900, 1, 1, 0), 1, 2, "111000000000000000000011"),
        (dt(1900, 1, 1, 0), 2, 0, "110000000000000000000000000000000000000000000000"),
        (dt(1900, 1, 1, 11), 2, 0, "000000000000000000000011000000000000000000000000"),
        (dt(1900, 1, 1, 11), 2, 3, "000000000000000000011111111000000000000000000000"),
        (dt(1900, 1, 1, 23), 2, 0, "000000000000000000000000000000000000000000000011"),
    ]

    @pytest.mark.parametrize("dt,cat_bits,bit_buf,expected", data)
    def test_encode(self, dt, cat_bits, bit_buf, expected):
        e = HourOfDayEncoder(cat_bits, bit_buf)
        actual = e.encode(dt)
        assert expected == actual.as_binary_string(e.num_bits)
