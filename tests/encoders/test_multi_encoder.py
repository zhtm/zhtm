import pytest


from zhtm.encoders.multi_encoder import MultiEncoder
from zhtm.encoders.scalar_encoder import ScalarEncoder


class TestScalarEncoder:
    def test_basic_combination(self):
        s1 = ScalarEncoder(0, 3, 3, 3)
        s2 = ScalarEncoder(0, 3, 3, 3)

        s1_binary = s1.encode(0).as_binary_string(s1.num_bits)
        s2_binary = s2.encode(2).as_binary_string(s2.num_bits)

        m = MultiEncoder([s1, s2])
        output_sdr = m.encode([0, 2])
        assert output_sdr.as_binary_string(m.num_bits) == s1_binary + s2_binary

    def test_num_inputs_mismatch(self):
        s1 = ScalarEncoder(0, 3, 3, 3)
        m = MultiEncoder([s1])

        with pytest.raises(ValueError):
            m.encode([1, 2])  # Too many inputs means raise an exception
