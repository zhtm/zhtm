import pytest

from zhtm.encoders.cycle_encoder import CycleEncoder, wrap_bounds


class TestWrapBounds:
    data = [
        # (i, min_i, max_i, expected_i)
        (50, 0, 100, 50),
        (0, 0, 100, 0),
        (100, 0, 100, 100),
        (-1, 0, 100, 100),
        (-2, 0, 50, 49),
        (21, 0, 20, 0),
        (22, 0, 20, 1),
        (20, 1, 10, 10),
        (30, 1, 10, 10),
        (31, 1, 10, 1),
        (-9, 1, 5, 1),
        (-4, 1, 5, 1),
        (-7, 1, 5, 3),
    ]

    @pytest.mark.parametrize("i,min_i,max_i,expected_i", data)
    def test_wrap_bounds(self, i, min_i, max_i, expected_i):
        actual = wrap_bounds(i, min_i, max_i)
        assert expected_i == actual


class TestCycleEncoder:
    def setup_method(self):
        self.categories = ["a", "b", "c"]
        self.bits_per_category = 9
        self.bit_buffer = 0
        self.encoder = CycleEncoder(
            self.categories, self.bits_per_category, self.bit_buffer
        )

    def test_basic_init(self):
        assert self.encoder.categories == self.categories
        assert self.encoder.bits_per_category == self.bits_per_category
        assert self.encoder.bit_buffer == self.bit_buffer
        assert self.encoder.num_bits_active == self.bits_per_category + (
            2 * self.bit_buffer
        )
        assert self.encoder.num_bits == len(self.categories) * self.bits_per_category

    data = [
        # ( categories, bits_per_category, bit_buffer, input, expected_sdr_string)
        (["a", "b", "c"], 5, 0, "a", "111110000000000"),
        (["a", "b", "c"], 5, 3, "b", "001111111111100"),
        (["a", "b", "c"], 5, 5, "b", "111111111111111"),
        (["a", "b", "c"], 5, 2, "a", "111111100000011"),
    ]

    @pytest.mark.parametrize("cats,cat_bits,buf,in_val,expect", data)
    def test_encode(self, cats, cat_bits, buf, in_val, expect):
        encoder = CycleEncoder(cats, cat_bits, buf)
        actual = encoder.encode(in_val)

        for bit_i in actual:
            # We can mistakenly pass this test if the 'wrap-around' bits
            # are not properly converted from below 0, because of how
            # list indexing with negative numbers interacts with the
            # convenience function `SDR.as_binary_string()`. Just be aware
            # that the following two SDRs will _appear_ to be the same:
            #   SDR(-1, 0).as_binary_string(5)
            #   SDR( 0, 4).as_binary_string(5)
            assert bit_i >= 0

        assert expect == actual.as_binary_string(encoder.num_bits)
