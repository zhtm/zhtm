import pytest

from zhtm.temporal_memory import (
    TemporalMemory,
    TMColumn,
    TMCell,
    TMDendriteSegment,
)
from zhtm.sdr import SDR


class TestTMDendriteSegment:
    def setup_method(self):
        self.seg = TMDendriteSegment("arbitrary_address")

    def test_init(self):
        address = "something hashable"
        segment = TMDendriteSegment(address)

        assert segment.address == address
        assert segment._potential_sdr == SDR()
        assert segment._connected_sdr == SDR()

        assert segment.is_active is False
        assert segment.is_matching is False

    handle_input_activate_data = (
        # (connected addresses, input addresses, active_threshold, expect_active)
        (SDR(), SDR(), 0, False),
        (SDR(), SDR(), 1, False),
        (SDR([1]), SDR(), 0, False),
        (SDR(), SDR([1]), 0, False),
        (SDR([1]), SDR([1]), 0, True),
        (SDR([1]), SDR([1]), 1, False),
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 0, True),  # (2, 3) overlap
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 1, True),  # (2, 3) overlap
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 2, False),  # (2, 3) overlap
        (SDR([1, 2, 3]), SDR([1, 2, 3, 4]), 2, True),  # (2, 3) overlap
    )

    @pytest.mark.parametrize(
        "connected,input_arg,active,expect_active", handle_input_activate_data
    )
    def test_handle_input_active_data(
        self, connected, input_arg, active, expect_active
    ):
        self.seg._connected_sdr = connected
        arbitrary_matching_threshold = 10
        is_active = self.seg.handle_input(
            input_arg, active, arbitrary_matching_threshold
        )
        assert is_active == expect_active

    handle_input_matching_data = (
        # (potential addresses, input addresses, matching_threshold, expect_matching, matching_synapse_count)
        (SDR(), SDR(), 0, False, 0),
        (SDR(), SDR(), 1, False, 0),
        (SDR([1]), SDR(), 0, False, 0),
        (SDR(), SDR([1]), 0, False, 0),
        (SDR([1]), SDR([1]), 0, True, 1),
        (SDR([1]), SDR([1]), 1, False, 1),
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 0, True, 2),
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 1, True, 2),
        (SDR([1, 2, 3]), SDR([2, 3, 4]), 2, False, 2),
    )

    @pytest.mark.parametrize(
        "potential,input_arg,matching,expect_match,match_count",
        handle_input_matching_data,
    )
    def test_handle_input_matching_data(
        self, potential, input_arg, matching, expect_match, match_count
    ):
        self.seg._potential_sdr = potential
        arbitrary_active_threshold = 10
        self.seg.handle_input(input_arg, arbitrary_active_threshold, matching)

        assert self.seg.is_matching == expect_match
        assert self.seg.num_active_potential_synapses == match_count

    def test_learn_calls_expected_functions(self, mocker):
        mocker.patch.object(self.seg, "_update_permanences")
        mocker.patch.object(self.seg, "_grow_new_synapses")
        mocker.patch.object(self.seg, "_reconcile_sdrs")

        fake_active = SDR([1, 2, 3])
        fake_winner = SDR([6, 7, 8])

        self.seg.learn(fake_active, fake_winner)

        self.seg._update_permanences.assert_called_with(fake_active)
        self.seg._grow_new_synapses.assert_called_with(fake_winner)
        self.seg._reconcile_sdrs.assert_called()

    def test_update_permanences_alters_expected_synapse_permanences(self):
        TMDendriteSegment.permanence_increment = 0.2
        TMDendriteSegment.permanence_decrement = 0.1
        TMDendriteSegment.default_permanence = 0.5

        previous_active_sdr = SDR([1, 2, 3])
        synapse_sdr = SDR([1, 2, 3, 4, 5, 6])
        self.seg.add_synapses(synapse_sdr)

        self.seg._update_permanences(previous_active_sdr)

        default_perm = self.seg.default_permanence
        for cell_address in self.seg._potential_permanences:
            permanence = self.seg._potential_permanences[cell_address]
            if cell_address in previous_active_sdr:
                assert permanence == default_perm + self.seg.permanence_increment
            else:
                assert permanence == default_perm - self.seg.permanence_decrement

    def test_update_permanences_clamps_values(self):
        TMDendriteSegment.permanence_increment = 10
        TMDendriteSegment.permanence_decrement = 10

        previous_active_sdr = SDR([1, 2, 3])
        synapse_sdr = SDR([1, 2, 3, 4, 5, 6])
        self.seg.add_synapses(synapse_sdr)
        self.seg._update_permanences(previous_active_sdr)

        for permanence in self.seg._potential_permanences.values():
            assert 0 <= permanence <= 1

    reconcile_sdr_data = (
        # (permanence_dict, expect_potential, expect_connected, expect_perm_dict)
        ({1: 0.5}, SDR([1]), SDR(), {1: 0.5}),  # perm not > connected threshold
        ({1: 0.6}, SDR([1]), SDR([1]), {1: 0.6}),
        ({1: 0.6, 2: 0.4}, SDR([1, 2]), SDR([1]), {1: 0.6, 2: 0.4}),
        ({1: 0, 2: 0.4}, SDR([2]), SDR(), {2: 0.4}),  # permanences of 0 are removed
    )

    @pytest.mark.parametrize("perm_dict,pot,con,e_perm", reconcile_sdr_data)
    def test_reconcile_sdrs(self, perm_dict, pot, con, e_perm):
        self.seg._potential_permanences = perm_dict

        self.seg._reconcile_sdrs()

        assert self.seg._potential_sdr == pot
        assert self.seg._connected_sdr == con
        assert self.seg._potential_permanences == e_perm

    def test_add_synapses_usual_cases(self):
        assert self.seg._potential_sdr == SDR()
        assert len(self.seg._potential_permanences) == 0

        address_sdr_1 = SDR([1, 2, 3])
        self.seg.add_synapses(address_sdr_1)

        assert self.seg._potential_sdr == address_sdr_1
        assert all(addr in address_sdr_1 for addr in self.seg._potential_permanences)
        assert all(
            perm == self.seg.default_permanence
            for perm in self.seg._potential_permanences.values()
        )

        address_sdr_2 = SDR([4, 5, 6])
        self.seg.add_synapses(address_sdr_2)
        assert self.seg._potential_sdr == SDR(address_sdr_1.union(address_sdr_2))

    def test_add_synapses_raises_exception_if_synapse_already_exists(self):
        address_sdr = SDR([1, 2, 3])
        self.seg.add_synapses(address_sdr)

        with pytest.raises(ValueError):
            self.seg.add_synapses(address_sdr)

    overlap_data = (
        # (num_candidates, max_synapses_per, num_synapses, overlap, expect_overun)
        (0, 10, 5, -5, False),
        (0, 10, 0, -10, False),
        (5, 10, 5, 0, False),
        (7, 10, 5, 2, True),
        (6, 5, 0, 1, True),
        (10, 10, 5, 5, True),
        (30, 10, 5, 15, True),  # self.new_synapse_sample_size respected
    )

    @pytest.mark.parametrize("num_candidates,max_syn,num_syn,overlap,expect_overlap", overlap_data)
    def test_prune_synapses_calls_destroy_during_overlap(self, mocker, num_candidates, max_syn, num_syn, overlap, expect_overlap):
        TMDendriteSegment.new_synapse_sample_size = 20
        TMDendriteSegment.max_synapses_per_segment = max_syn
        d = TMDendriteSegment('fake_address')
        mocker.patch.object(d, 'destroy_min_permanence_synapses')

        d._potential_permanences = {i: 0.5 for i in range(num_syn)}
        fake_winners = SDR(list(range(num_candidates)))
        d._prune_synapses_for_growth(num_candidates, fake_winners)

        if expect_overlap:
            d.destroy_min_permanence_synapses.assert_called_with(overlap, fake_winners)
        else:
            d.destroy_min_permanence_synapses.assert_not_called()

    grow_synapse_data = (
        # (prev_winners, potentials, candidate_count, expected_synapse_count)
        (SDR([]), SDR([1, 2, 3]), 0, 3),
        (SDR([1]), SDR([1, 2, 3]), 0, 3),
        (SDR([1]), SDR([2, 3]), 1, 3),
        (SDR([1, 2, 3]), SDR([]), 3, 3),
        (SDR([1, 2, 3]), SDR([4, 5, 6]), 3, 5)  # respect max_synapses_per_segment
    )

    @pytest.mark.parametrize("prev_winners,potentials,candidate_count,expected", grow_synapse_data)
    def test_grow_new_synapses_expected_count(self, mocker, prev_winners, potentials, candidate_count, expected):
        TMDendriteSegment.max_synapses_per_segment = 5
        d = TMDendriteSegment('fake_address')
        d._potential_permanences = {i: 0.1 * (i+1) for i in potentials}
        d._reconcile_sdrs()

        mocker.spy(d, '_prune_synapses_for_growth')

        d._grow_new_synapses(prev_winners)

        d._prune_synapses_for_growth.assert_called_with(candidate_count, prev_winners)
        assert len(d._potential_permanences) == expected

    def test_punish_adjusts_expected_permanences(self):
        TMDendriteSegment.predicted_decrement = 0.1
        TMDendriteSegment.default_permanence = 0.5
        punished_permanence = 0.4

        potential_sdr = SDR([1, 2, 3, 4, 5])
        for i in potential_sdr:
            self.seg.add_synapses([i])

        previous_active_sdr = SDR([1, 3, 5])
        self.seg.punish(previous_active_sdr)

        for i in potential_sdr:
            actual_permanence = self.seg._potential_permanences[i]
            if i in previous_active_sdr:
                assert actual_permanence == punished_permanence
            else:
                assert actual_permanence == self.seg.default_permanence

    def test_punish_reconciles_sdrs(self, mocker):
        mocker.patch.object(self.seg, "_reconcile_sdrs")
        self.seg.punish(SDR())
        self.seg._reconcile_sdrs.assert_called()

    def test_punish_clamps_values(self):
        self.seg.predicted_decrement = 1000.0
        fake_address = "arbitrary address"
        self.seg.add_synapses([fake_address])

        assert all(
            [0 <= perm <= 1 for perm in self.seg._potential_permanences.values()]
        )
        self.seg.punish(SDR([fake_address]))
        assert all(
            [0 <= perm <= 1 for perm in self.seg._potential_permanences.values()]
        )

    destroy_synapses_data = (
        # (permanences, exclude, num_destroy, expected_count, expected_min_perm)
        # NOTE: if two synapses have identical permanences, the synapse with the
        # 'lesser' address will be destroyed first. Does that matter?
        ((0.5, 0.6, 0.7), (), 1, 2, 0.6),
        ((0.7, 0.6, 0.5), (), 1, 2, 0.6),
        ((0.7, 0.6, 0.5), (2,), 1, 2, 0.5),  # Keep address 2 (perm==0.5)
        ((0.7, 0.6, 0.5), (), 2, 1, 0.7),
        ((0.7, 0.6, 0.5), (), 3, 0, None),
        ((0.7, 0.6, 0.5), (1, 2), 1, 2, 0.5),  # Keep address 1 and 2
        (
            (0.7, 0.6, 0.5),
            (0, 1, 2),
            3,
            3,
            0.5,
        ),  # 'exclude_cells' wins over 'num_to_destroy'
        ((0.5, 0.6, 0.7), (), 10, 0, None),  # num_destroy > num available, no error
    )

    @pytest.mark.parametrize(
        "perms,exclude,num,expect_c,expect_min", destroy_synapses_data
    )
    def test_destroy_min_permanence_synapses(
        self, perms, exclude, num, expect_c, expect_min
    ):
        d = TMDendriteSegment("fake_address")
        d._potential_permanences = {i: p for i, p in enumerate(perms)}
        d.destroy_min_permanence_synapses(num, exclude)

        if expect_min is not None:
            min_perm = min(d._potential_permanences.values())
            assert min_perm == expect_min
        assert len(d._potential_permanences) == expect_c
        assert (
            len(d._potential_sdr) == expect_c
        )  # Make sure _reconcile_sdrs() is called


class TestTMCell:
    def setup_method(self):
        TMCell.learning_enabled = False

        fake_column_address = (1, 2)
        fake_cell_address = (fake_column_address, 0)

        self.cell = TMCell(fake_cell_address)

        # Add a few empty dendrite segments for testing
        num_dendrites = 4
        for i in range(num_dendrites):
            dendrite_address = (self.cell.address, i)
            self.cell.distal_segments[dendrite_address] = TMDendriteSegment(
                dendrite_address
            )

    def test_init(self):
        fake_column_address = (1, 2)
        fake_cell_address = (fake_column_address, 0)
        cell = TMCell(fake_cell_address)

        assert cell.address == fake_cell_address
        assert len(cell.distal_segments) == 0

    @pytest.mark.parametrize("is_active", [True, False])
    def test_predict_next_input_calls_handle_input_on_segments(self, mocker, is_active):
        fake_activation = 20
        fake_matching = 10
        TMCell.distal_activation_threshold = fake_activation
        TMCell.distal_matching_threshold = fake_matching

        for segment in self.cell.distal_segments.values():
            mocker.patch.object(segment, "handle_input")
            segment.handle_input.return_value = is_active

        arbitrary_input = SDR([1, 2, 3, 4])
        cell_active_sdr = self.cell.predict_next_input(arbitrary_input)

        for segment in self.cell.distal_segments.values():
            segment.handle_input.assert_called_with(
                arbitrary_input, fake_activation, fake_matching
            )

        if is_active:
            assert cell_active_sdr == SDR([self.cell.address])
        else:
            assert cell_active_sdr == SDR()

    @pytest.mark.parametrize("segment_active", [True, False])
    def test_handle_active_and_predicted_returns_expected_values(self, segment_active):
        segment_addresses = list(self.cell.distal_segments.keys())
        for address in segment_addresses:
            if address == segment_addresses[-1]:
                self.cell.distal_segments[address].is_active = segment_active
            else:
                self.cell.distal_segments[address].is_active = False

        fake_sdr = SDR()
        cell_active = self.cell.handle_active_and_predicted(fake_sdr, fake_sdr)

        assert cell_active == segment_active

    def test_handle_active_and_predicted_respects_learning_flag(self, mocker):
        fake_active = SDR([1, 2, 3])
        fake_winner = SDR([6, 7, 8])

        active_segments = SDR()
        for i, segment in enumerate(self.cell.distal_segments.values()):
            mocker.patch.object(segment, "learn")
            if i % 2 == 0:
                segment.is_active = True
                active_segments.update([segment.address])
            else:
                segment.is_active = False

        # With learning disabled, no segments should learn
        TMCell.learning_enabled = False
        self.cell.handle_active_and_predicted(fake_active, fake_winner)
        for segment in self.cell.distal_segments.values():
            segment.learn.assert_not_called()

        # With learning enabled, active segments should learn
        TMCell.learning_enabled = True
        self.cell.handle_active_and_predicted(fake_active, fake_winner)
        for segment in self.cell.distal_segments.values():
            if segment.address in active_segments:
                segment.learn.assert_called_with(fake_active, fake_winner)
            else:
                segment.learn.assert_not_called()

    def test_get_best_matching_segment_base_case(self):
        seg1 = TMDendriteSegment("seg1")
        seg2 = TMDendriteSegment("seg2")
        seg3 = TMDendriteSegment("seg3")
        seg4 = TMDendriteSegment("seg4")

        seg1.is_matching = False
        seg1.num_active_potential_synapses = 3

        seg2.is_matching = True
        seg2.num_active_potential_synapses = 10

        seg3.is_matching = True
        seg3.num_active_potential_synapses = 9

        # This case shouldn't be possible (`is_matching` = False with highest num_active_potential_synapses),
        # but this case is here just to make sure we are filtering out non-matching segments.
        seg4.is_matching = False
        seg4.num_active_potential_synapses = 1000

        self.cell.distal_segments = {x.address: x for x in [seg1, seg2, seg3]}

        best_score, best_segment = self.cell.get_best_matching_segment()
        assert best_score == 10
        assert best_segment == seg2

    def test_grow_new_segment(self):
        self.cell.distal_segments = dict()

        expected_address_0 = (self.cell.address, 0)
        expected_address_1 = (self.cell.address, 1)

        assert len(self.cell.distal_segments) == 0
        assert expected_address_0 not in self.cell.distal_segments
        assert expected_address_1 not in self.cell.distal_segments

        return_segment = self.cell.grow_new_segment()

        assert return_segment.address == expected_address_0
        assert len(self.cell.distal_segments) == 1
        assert expected_address_0 in self.cell.distal_segments
        assert expected_address_1 not in self.cell.distal_segments

        return_segment = self.cell.grow_new_segment()
        assert return_segment.address == expected_address_1
        assert len(self.cell.distal_segments) == 2
        assert expected_address_0 in self.cell.distal_segments
        assert expected_address_1 in self.cell.distal_segments

    def test_punish_matching_segments(self, mocker):
        expecting_punishment = SDR()
        for i, segment in enumerate(self.cell.distal_segments.values()):
            mocker.patch.object(segment, "punish")
            if i % 2 == 0:
                segment.is_matching = True
                expecting_punishment.update([segment.address])
            else:
                segment.is_matching = False

        fake_previous_active_cells = SDR([2, 3, 4])
        self.cell.punish_matching_segments(fake_previous_active_cells)

        for segment in self.cell.distal_segments.values():
            if segment.address in expecting_punishment:
                segment.punish.assert_called_with(fake_previous_active_cells)
            else:
                segment.punish.assert_not_called()


class TestTMColumn:
    def setup_method(self):
        column_address = 1
        TMColumn.num_cells = 5

        self.c = TMColumn(column_address)

    def test_init(self):
        fake_address = "the_column"  # anything hashable
        TMColumn.num_cells = 10

        c = TMColumn(fake_address)

        assert c.address == fake_address
        assert len(c.cells) == TMColumn.num_cells
        for cell_address, cell in self.c.cells.items():
            assert cell.address == cell_address
            assert cell.address[0] == self.c.address
            assert isinstance(cell, TMCell)

    def test_predict_next_input_calls_predict_on_all_cells(self, mocker):
        for cell in self.c.cells.values():
            mocker.patch.object(cell, "predict_next_input")

        active_cell_sdr = SDR([1, 3, 5, 6])
        self.c.predict_next_input(active_cell_sdr)

        for cell in self.c.cells.values():
            cell.predict_next_input.assert_called_with(active_cell_sdr)

    def test_predict_next_input_sets_active_cell_sdr(self, mocker):
        arbitrary_input = SDR([1, 2, 3])
        expected_active_cell_sdr = SDR()

        for i, address in enumerate(self.c.cells):
            cell = self.c.cells[address]
            mocker.patch.object(cell, "predict_next_input")
            if i % 2 == 0:
                expected_active_cell_sdr.update([address])
                cell.predict_next_input.return_value = SDR([address])
            else:
                cell.predict_next_input.return_value = SDR()

        assert self.c.active_cell_sdr == SDR()
        col_sdr = self.c.predict_next_input(arbitrary_input)
        assert self.c.active_cell_sdr == expected_active_cell_sdr
        assert col_sdr == SDR([self.c.address])

    def test_reset_clears_appropriate_data(self):
        self.c.active_cell_sdr = SDR([1, 2, 3, 4])
        self.c.reset_pattern()
        assert self.c.active_cell_sdr == SDR()

    def test_handle_active_and_predicted_calls_cells_appropriately(self, mocker):
        expected_active_cells = SDR()
        for i, cell in enumerate(self.c.cells.values()):
            mocker.patch.object(cell, "handle_active_and_predicted")

            if i % 2 == 0:
                expected_active_cells.update([cell.address])
                cell.handle_active_and_predicted.return_value = True
            else:
                cell.handle_active_and_predicted.return_value = False

        fake_previous_active = SDR([1, 2, 3])
        fake_previous_winner = SDR([2, 3, 4])

        actual_active_sdr = self.c.handle_active_and_predicted(
            fake_previous_active, fake_previous_winner
        )

        assert actual_active_sdr == expected_active_cells
        for cell in self.c.cells.values():
            cell.handle_active_and_predicted.assert_called_with(
                fake_previous_active, fake_previous_winner
            )

    def test_handle_active_and_unpredicted_calls_expected_funcs(self, mocker):
        TMCell.learning_enabled = False

        fake_winner_cell_sdr = SDR([-100])
        fake_segment = TMDendriteSegment("some_address")
        mocker.patch.object(fake_segment, "learn")

        mocker.patch.object(self.c, "_get_bursted_cell_and_segment")
        self.c._get_bursted_cell_and_segment.return_value = (
            fake_winner_cell_sdr,
            fake_segment,
        )

        prev_active = SDR([1, 2, 3])
        prev_winner = SDR([2, 4, 6])
        actual_active, actual_winner = self.c.handle_active_and_unpredicted(
            prev_active, prev_winner
        )

        assert actual_active == SDR(self.c.cells.keys())
        assert actual_winner == fake_winner_cell_sdr

        self.c._get_bursted_cell_and_segment.assert_called()
        fake_segment.learn.assert_not_called()

        TMCell.learning_enabled = True
        self.c.handle_active_and_unpredicted(prev_active, prev_winner)
        fake_segment.learn.assert_called_with(prev_active, prev_winner)

    def test_get_bursted_cell_and_segment_least_used_cell(self, mocker):
        cell_1 = TMCell("cell_1")
        cell_2 = TMCell("cell_2")
        cell_3 = TMCell("cell_3")

        cell_list = [cell_1, cell_2, cell_3]
        for c in cell_list:
            # Make 'best_matching_segment' return Nones so we enter the 'least used cell' logic
            mocker.patch.object(c, "get_best_matching_segment")
            c.get_best_matching_segment.return_value = (None, None)

        # 'least used cell' is based on number of dendrite segments,
        # so we fake it by putting arbitrary objects in the `distal_segment` dicts
        cell_1.distal_segments = dict(a=1, b=2)
        cell_2.distal_segments = dict(a=1, b=2, c=3)
        cell_3.distal_segments = dict(a=1, b=2)

        self.c.cells = {x.address: x for x in [cell_1, cell_2, cell_3]}

        TMCell.learning_enabled = False
        winning_sdr, learning_segment = self.c._get_bursted_cell_and_segment()
        assert learning_segment is None

        TMCell.learning_enabled = True
        winning_sdr, learning_segment = self.c._get_bursted_cell_and_segment()
        assert learning_segment is not None

        # Show that winning cell is one of the two cells with the lowest number of distal segments
        winning_address = winning_sdr.pop()
        assert winning_address in [cell_1.address, cell_3.address]

        # Show that the `learning_segment` is empty
        assert len(learning_segment._potential_permanences) == 0

        allowed_segments = [x for x in cell_1.distal_segments] + [
            x for x in cell_3.distal_segments
        ]
        assert learning_segment.address in allowed_segments

    def test_get_bursted_cell_and_segment_returns_winning_cell(self, mocker):
        cell_1 = TMCell("cell_1")
        cell_2 = TMCell("cell_2")
        cell_3 = TMCell("cell_3")

        cell_list = [cell_1, cell_2, cell_3]
        for i, c in enumerate(cell_list):
            # Make 'best_matching_segment' return Nones so we enter the 'least used cell' logic
            mocker.patch.object(c, "get_best_matching_segment")
            segment = c.grow_new_segment()

            # Score == i, so last cell in `cell_list` is expected to be 'winning' cell
            c.get_best_matching_segment.return_value = (i, segment)
        self.c.cells = {x.address: x for x in [cell_1, cell_2, cell_3]}

        winning_cell_sdr, learning_segment = self.c._get_bursted_cell_and_segment()
        assert winning_cell_sdr == SDR([cell_3.address])
        assert learning_segment.address in cell_3.distal_segments

    def test_handle_inactive_and_predicted(self, mocker):
        for cell in self.c.cells.values():
            mocker.patch.object(cell, "punish_matching_segments")

        fake_cell_sdr = SDR([1, 2, 3])

        self.c.handle_inactive_and_predicted(fake_cell_sdr)

        for cell in self.c.cells.values():
            cell.punish_matching_segments.assert_called_with(fake_cell_sdr)


class TestTemporalMemory:
    def setup_method(self):
        column_addresses = range(10)
        self.tm = TemporalMemory(column_addresses)

    def test_init(self):
        assert isinstance(self.tm, TemporalMemory)

        assert len(self.tm.columns) > 0
        for address, col in self.tm.columns.items():
            assert isinstance(col, TMColumn)
            assert col.address == address

    def test_set_column_buckets_detects_correct_column_states(self):

        spatial_pooler_columns = SDR([1, 2, 3, 4, 5])
        fake_predicted_columns = SDR([3, 4, 5, 6, 7])

        expected_active_and_predicted = SDR([3, 4, 5])
        expected_active_and_unpredicted = SDR([1, 2])
        expected_inactive_and_predicted = SDR([6, 7])

        self.tm._col_predicted_sdr = fake_predicted_columns

        self.tm._set_column_buckets(spatial_pooler_columns)
        assert self.tm._col_active_predicted_sdr == expected_active_and_predicted
        assert self.tm._col_active_unpredicted_sdr == expected_active_and_unpredicted
        assert self.tm._col_inactive_predicted_sdr == expected_inactive_and_predicted

    def test_process_input_calls_expected_functions(self, mocker):
        spatial_pooler_sdr = SDR([1, 2, 3, 4, 5, 6, 7, 8, 9])

        mocker.patch.object(self.tm, "_set_column_buckets")
        mocker.patch.object(self.tm, "_handle_active_predicted_columns")
        mocker.patch.object(self.tm, "_handle_active_unpredicted_columns")
        mocker.patch.object(self.tm, "_handle_inactive_predicted_columns")
        mocker.patch.object(self.tm, "_predict_next_input")

        self.tm._handle_active_predicted_columns.return_value = (SDR(), SDR())
        self.tm._handle_active_unpredicted_columns.return_value = (SDR(), SDR())

        self.tm.process_input(spatial_pooler_sdr)

        self.tm._set_column_buckets.assert_called_with(spatial_pooler_sdr)
        self.tm._handle_active_predicted_columns.assert_called()
        self.tm._handle_active_unpredicted_columns.assert_called()
        self.tm._handle_inactive_predicted_columns.assert_called()
        self.tm._predict_next_input.assert_called()

    def test_process_input_updates_active_and_winner_cells(self, mocker):
        mocker.patch.object(self.tm, "_handle_active_predicted_columns")
        mocker.patch.object(self.tm, "_handle_active_unpredicted_columns")

        for col in self.tm.columns.values():
            mocker.patch.object(col, "predict_next_input")
            col.predict_next_input.return_value = SDR()

        self.tm._handle_active_predicted_columns.return_value = SDR([1]), SDR([10])
        self.tm._handle_active_unpredicted_columns.return_value = SDR([2]), SDR([20])
        expected_active = SDR([1, 2])
        expected_winner = SDR([10, 20])

        arbitrary_input = SDR([1, 2, 3, 4, 5])

        self.tm.process_input(arbitrary_input)

        assert self.tm._stored_active_cell_sdr == expected_active
        assert self.tm._stored_winner_cell_sdr == expected_winner

    def test_handle_active_predicted_columns_affects_expected_columns(self, mocker):
        affected_columns = SDR([1, 3, 5])
        stored_active_sdr = SDR([5, 6, 7])
        stored_winner_sdr = SDR([7, 8, 9])

        expected_active_sdr = SDR(["active_cell"])
        expected_winner_sdr = SDR(["active_cell"])  # yes, 'winner' is same as 'active'

        for address, col in self.tm.columns.items():
            mocker.patch.object(col, "handle_active_and_predicted")
            if address in affected_columns:
                col.handle_active_and_predicted.return_value = SDR(["active_cell"])

        # TM should call function on all columns in tm._col_active_predicted_sdr
        self.tm._col_active_predicted_sdr = affected_columns
        self.tm._stored_active_cell_sdr = stored_active_sdr
        self.tm._stored_winner_cell_sdr = stored_winner_sdr

        actual_active, actual_winner = self.tm._handle_active_predicted_columns()

        for address, col in self.tm.columns.items():
            if address in affected_columns:
                col.handle_active_and_predicted.assert_called_with(
                    stored_active_sdr, stored_winner_sdr
                )
            else:
                col.handle_active_and_predicted.assert_not_called()

        assert actual_active == expected_active_sdr
        assert actual_winner == expected_winner_sdr

    def test_handle_active_unpredicted_columns_bursts_affected_columns(self, mocker):
        affected_columns = SDR([2, 3, 4])
        stored_active_sdr = SDR([5, 6, 7])
        stored_winner_sdr = SDR([7, 8, 9])

        expected_active_sdr = SDR(["active_cell"])
        expected_winner_sdr = SDR(["winner_cell"])

        for address, col in self.tm.columns.items():
            mocker.patch.object(col, "handle_active_and_unpredicted")
            if address in affected_columns:
                col.handle_active_and_unpredicted.return_value = (
                    SDR(["active_cell"]),
                    SDR(["winner_cell"]),
                )

        # TM should call function on all columns in tm._col_active_unpredicted_sdr
        self.tm._col_active_unpredicted_sdr = affected_columns
        self.tm._stored_active_cell_sdr = stored_active_sdr
        self.tm._stored_winner_cell_sdr = stored_winner_sdr
        actual_active, actual_winner = self.tm._handle_active_unpredicted_columns()

        for address, col in self.tm.columns.items():
            if address in affected_columns:
                col.handle_active_and_unpredicted.assert_called_with(
                    stored_active_sdr, stored_winner_sdr
                )
            else:
                col.handle_active_and_unpredicted.assert_not_called()

        assert actual_active == expected_active_sdr
        assert actual_winner == expected_winner_sdr

    def test_handle_inactive_predicted_columns_affects_expected_columns(self, mocker):
        affected_columns = SDR([9, 7, 1])
        fake_active_cell_sdr = SDR([1, 3, 5, 7, 9])

        for address, col in self.tm.columns.items():
            mocker.patch.object(col, "handle_inactive_and_predicted")

        self.tm._col_inactive_predicted_sdr = affected_columns
        self.tm._stored_active_cell_sdr = fake_active_cell_sdr

        TMCell.learning_enabled = False
        self.tm._handle_inactive_predicted_columns()
        for col in self.tm.columns.values():
            col.handle_inactive_and_predicted.assert_not_called()

        TMCell.learning_enabled = True
        self.tm._handle_inactive_predicted_columns()
        for address, col in self.tm.columns.items():
            if address in affected_columns:
                col.handle_inactive_and_predicted.assert_called_with(
                    fake_active_cell_sdr
                )
            else:
                col.handle_inactive_and_predicted.assert_not_called()

    def test_predict_next_input_calls_all_columns(self, mocker):
        fake_active_cells = "Doesn't really matter what this is"
        for col in self.tm.columns.values():
            mocker.patch.object(col, "predict_next_input")

        self.tm._predict_next_input(fake_active_cells)

        for col in self.tm.columns.values():
            col.predict_next_input.assert_called_with(fake_active_cells)

    def test_predict_next_input_assembles_predicted_column_sdr(self, mocker):
        expected_sdr = SDR()
        for i, address in enumerate(self.tm.columns):
            col = self.tm.columns[address]
            mocker.patch.object(col, "predict_next_input")
            if i % 2 == 0:
                col.predict_next_input.return_value = SDR([address])
                expected_sdr.update([address])
            else:
                col.predict_next_input.return_value = SDR()

        self.tm._predict_next_input(SDR())

        assert self.tm._col_predicted_sdr == expected_sdr

    def test_reset_pattern_clears_expected_values(self, mocker):
        self.tm._col_predicted_sdr = SDR([1, 2, 3, 4])
        self.tm._stored_winner_cell_sdr = SDR([1, 2, 3, 4])
        self.tm._stored_active_cell_sdr = SDR([1, 2, 3, 4])

        for col in self.tm.columns.values():
            mocker.patch.object(col, "reset_pattern")

        self.tm.reset_pattern()

        assert self.tm._col_predicted_sdr == SDR()
        assert self.tm._stored_winner_cell_sdr == SDR()
        assert self.tm._stored_active_cell_sdr == SDR()

        for col in self.tm.columns.values():
            col.reset_pattern.assert_called()
