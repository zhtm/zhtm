import pytest

from zhtm.utilities import clamp

clamp_data = [
    # (val, min, max, expected)
    (5, 0, 10, 5),
    (0, 0, 10, 0),
    (0.0, 0, 10, 0),
    (1.0, 0, 1, 1.0),
    (-1, 0, 1, 0),
    (2, 0, 1, 1),
]


@pytest.mark.parametrize("val, min_val, max_val, expected", clamp_data)
def test_clamp(val, min_val, max_val, expected):
    actual = clamp(val, min_val, max_val)
    assert expected == actual
