import pytest

from zhtm.sdr import SDR
from zhtm.dendrite_segment import DendriteSegment
from zhtm.sp_column import SPColumn


class TestSPColumn:
    def setup_method(self):
        self.c = SPColumn("fake_address")

    def test_basic_init(self):
        arbitrary_hashable_address = "fake_address"
        c = SPColumn(arbitrary_hashable_address)
        assert c.address == arbitrary_hashable_address
        assert isinstance(c.proximal_segment, DendriteSegment)
        assert c.boost_multiplier == 1.0

    def test_randomize_potential_synapses(self, mocker):
        mocker.patch.object(self.c.proximal_segment, "randomize_potential_synapses")

        num_synapses = 100
        input_address_space = 10000
        self.c.randomize_potential_synapses(num_synapses, input_address_space)
        self.c.proximal_segment.randomize_potential_synapses.assert_called_with(
            num_synapses, input_address_space
        )

    def test_process_input_sdr(self, mocker):
        mocker.patch.object(self.c.proximal_segment, "process_input")
        input_sdr = SDR.random()
        self.c.process_input(input_sdr)
        self.c.proximal_segment.process_input.assert_called_with(input_sdr)

    boosted_count_data = [
        # (active_connected_indices, boost_value, expected_num_boosted_actives)
        ({1}, 1.0, 1),
        ({1, 2}, 1.0, 2),
        ({1}, 1.5, 1.5),
        ({1, 2}, 1.5, 3.0),
    ]

    @pytest.mark.parametrize("active, boost, expected_count", boosted_count_data)
    def test_boosted_synapse_count(self, active, boost, expected_count):
        self.c.proximal_segment.active_connected_sdr = SDR(active)
        self.c.boost_multiplier = boost  # set manually for this test
        actual_count = self.c.get_boosted_count()
        assert actual_count == expected_count

    def test_adapt_permanences(self, mocker):
        mocker.patch.object(self.c.proximal_segment, "adjust_active_permanences")
        mocker.patch.object(self.c.proximal_segment, "adjust_inactive_permanences")

        self.c.adjust_permanences()

        self.c.proximal_segment.adjust_active_permanences.assert_called_with(
            self.c.permanence_increment
        )
        self.c.proximal_segment.adjust_inactive_permanences.assert_called_with(
            -self.c.permanence_decrement
        )

    active_duty_data = [
        # (num_cycles, current_duty_cycle, column_is_active, expected_duty_cycle)
        (1, 0.0, True, 1.0),
        (1, 0.0, False, 0.0),
        (2, 1.0, True, 1.0),
        (2, 1.0, False, 0.5),
        (2, 0.0, False, 0.0),
        (10, 1.0, False, 0.9),
        (1001, 1.0, True, 1.0),
        (9001, 1.0, False, 0.999),  # cycles capped at 1000 for calculation
    ]

    @pytest.mark.parametrize("cycles, current, is_active, expected", active_duty_data)
    def test_update_active_duty_cycle(self, cycles, current, is_active, expected):
        self.c.active_duty_cycle = current

        self.c.update_active_duty_cycle(cycles, is_active)

        assert expected == self.c.active_duty_cycle

    overlap_duty_data = [
        # (num_cycles, current_duty_cycle, column active indices, stimulus threshold, expected duty cycle)
        (1, 0.0, False, 0.0),
        (1, 0.0, True, 1.0),
        (2, 0.0, False, 0.0),
        (2, 0.0, True, 0.5),
        (2, 1.0, False, 0.5),
        (2, 1.0, True, 1.0),
        (9000, 1.0, True, 1.0),
        (9000, 1.0, False, 0.999),  # cycles capped at 1000 for calculation
    ]

    @pytest.mark.parametrize(
        "cycles,cur_duty,is_overlapping,expected_duty", overlap_duty_data
    )
    def test_update_overlap_duty_cycle(
        self, mocker, cycles, cur_duty, is_overlapping, expected_duty
    ):
        self.c.overlap_duty_cycle = cur_duty

        mocker.patch.object(self.c.proximal_segment, "is_active")
        self.c.proximal_segment.is_active.return_value = is_overlapping

        self.c.update_overlap_duty_cycle(cycles)

        self.c.proximal_segment.is_active.assert_called_with(self.c.stimulus_threshold)
        assert expected_duty == self.c.overlap_duty_cycle

    def test_increase_all_permanences(self, mocker):
        mocker.patch.object(self.c.proximal_segment, "adjust_active_permanences")
        mocker.patch.object(self.c.proximal_segment, "adjust_inactive_permanences")

        self.c.increase_all_permanences()

        self.c.proximal_segment.adjust_active_permanences.assert_called_with(
            self.c.overlap_duty_cycle_low_increment
        )
        self.c.proximal_segment.adjust_inactive_permanences.assert_called_with(
            self.c.overlap_duty_cycle_low_increment
        )

    active_adjustment_data = [
        # (active_duty_cycle, desired_duty_cycle, boost_strength, multiplier compared to 1.0)
        (0.1, 0.2, 0.0, "equal"),  # boost_strength == 0 so boost_multiplier = 1
        (0.1, 0.1, 0.0, "equal"),  # active == desired, so boost_multiplier = 1
        (0.1, 0.2, 0.1, "greater"),  # duty cycle too low, boost multiplier > 1.0
        (0.2, 0.1, 0.1, "less"),  # duty cycle too high, boost multiplier < 1.0
    ]

    @pytest.mark.parametrize(
        "active,desired,strength,compare_one", active_adjustment_data
    )
    def test_adjust_boost_multiplier(self, active, desired, strength, compare_one):
        self.c.active_duty_cycle = active
        self.c.adjust_boost_multiplier(desired, strength)

        if compare_one == "equal":
            assert 1.0 == self.c.boost_multiplier
        elif compare_one == "greater":
            assert 1.0 < self.c.boost_multiplier
        elif compare_one == "less":
            assert 1.0 > self.c.boost_multiplier
        else:
            raise ValueError("Unknown comparison with one: {}".format(compare_one))
