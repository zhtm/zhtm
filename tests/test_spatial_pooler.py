import pytest

from zhtm.sp_column import SPColumn
from zhtm.spatial_pooler import NaiveSpatialPooler
from zhtm.sdr import SDR


class TestNaiveRegion:
    def setup_method(self):
        self.num_input_bits = 100
        self.num_columns = 100
        self.num_potential_synapses_per_column = 20
        self.n = NaiveSpatialPooler(self.num_input_bits, self.num_columns)
        self.n.randomize_column_synapses(self.num_potential_synapses_per_column)

    def test_init(self):
        num_bits = 10
        num_columns = 20
        n = NaiveSpatialPooler(num_bits, num_columns)

        assert n.num_input_bits == num_bits
        assert num_columns == len(n.columns)
        for c in n.columns.values():
            assert isinstance(c, SPColumn)

    def test_initialize_column_synapses(self, mocker):
        num_potential_synapses_per_column = 10

        for address in self.n.columns:
            mocker.patch.object(self.n.columns[address], "randomize_potential_synapses")
        self.n.randomize_column_synapses(num_potential_synapses_per_column)
        for address, c in self.n.columns.items():
            c.randomize_potential_synapses.assert_called_with(
                num_potential_synapses_per_column, self.n.num_input_bits
            )

    def test_send_input_to_columns(self, mocker):
        for c in self.n.columns.values():
            mocker.patch.object(c, "process_input")
            mocker.patch.object(c, "get_boosted_count")
            c.get_boosted_count.return_value = c.address * 2

        expected_boosted_pairs = [
            (c.address * 2, c.address) for c in self.n.columns.values()
        ]

        input_sdr = SDR.random()
        boosted_value_pairs = self.n.send_input_to_columns(input_sdr)

        for c in self.n.columns.values():
            # Every column had `process_input()` called
            c.process_input.assert_called_with(input_sdr)

        assert expected_boosted_pairs == boosted_value_pairs

    winning_col_data = [
        # (boost_pairs, stimulus_threshold, num_winners, expected_winners)
        (
            [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)],
            0,  # stimulus threshold
            3,  # number of winners
            {4, 3, 2},  # expected winning column indices
        ),
        (
            [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)],
            2,  # stimulus threshold
            3,  # number of winners
            {4, 3, 2},  # expected winning column indices
        ),
        (
            [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)],
            4,  # stimulus threshold
            3,  # number of winners
            {4},  # expected winning column indices
        ),
        (
            [(0, 0), (1, 1), (2, 2), (3, 3), (4, 4)],
            5,  # stimulus threshold
            3,  # number of winners
            set(),  # expected winning column indices
        ),
    ]

    @pytest.mark.parametrize("boost_pairs,stim,win_n,expected", winning_col_data)
    def test_get_winning_columns(self, boost_pairs, stim, win_n, expected):
        self.n.stimulus_threshold = stim
        self.n.winning_column_count = win_n

        actual_winners = self.n.get_winning_columns(boost_pairs)

        assert expected == actual_winners

    def test_adapt_winning_columns(self, mocker):
        full_sdr = SDR.random(self.n.num_input_bits, self.n.num_input_bits)
        winning_addresses = {1, 3, 5}

        for c in self.n.columns.values():
            mocker.patch.object(c, "adjust_permanences")

        self.n.adapt_winning_columns(winning_addresses)

        for address, c in self.n.columns.items():
            if address in winning_addresses:
                c.adjust_permanences.assert_called()
            else:
                c.adjust_permanences.assert_not_called()

    def test_update_column_duty_cycles(self, mocker):
        self.n.cycle_count = 3

        for c in self.n.columns.values():
            mocker.patch.object(c, "update_overlap_duty_cycle")
            mocker.patch.object(c, "update_active_duty_cycle")
            c.overlap_duty_cycle = 0.98

        # Arbitrarily decide that column 4 'won', aka was `active` after inhibition.
        winning_addresses = {4}
        max_overlap = self.n.update_column_duty_cycles(winning_addresses)

        for address, c in self.n.columns.items():
            if address in winning_addresses:
                c.update_active_duty_cycle.assert_called_with(self.n.cycle_count, True)
            else:
                c.update_active_duty_cycle.assert_called_with(self.n.cycle_count, False)

        assert max_overlap == 0.98

    # TODO: totally missing tests for `adjust_column_duty_cycle_contributors()`?

    def test_process_input_increases_cycle_count(self):
        random_input_sdr = SDR.random(self.n.num_input_bits, 10)

        for i in range(10):
            assert i == self.n.cycle_count
            self.n.process_input(random_input_sdr)
            assert i + 1 == self.n.cycle_count

    def test_process_input_returns_right_sized_sdr(self):
        # Run process_input() a few times to make sure it works repeatedly
        for _ in range(10):
            random_input_sdr = SDR.random(self.n.num_input_bits, 10)
            output_sdr = self.n.process_input(random_input_sdr)

            assert isinstance(output_sdr, SDR)
            assert self.n.winning_column_count == len(output_sdr)
