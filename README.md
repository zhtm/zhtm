# ZHTM
Zachary's HTM implementation.  All Python, all the time.

This is an implementation of the fundamental [Hierarchical Temporal Memory](https://numenta.org/hierarchical-temporal-memory/)
building blocks, namely Sparse Distributed Representations, Spatial Pooling, and Temporal Memory.

This project is _rather young_. Don't be surprised if sweeping architectural changes happen.

## Quickstart

Using Python 3:

    git clone git@bitbucket.org:zhtm/zhtm.git
    cd zhtm
    python3 -m venv venv
    source venv/bin/activate
    pip install --upgrade pip
    pip install --upgrade setuptools
    pip install -r requirements.txt
    python -m pytest tests/

## Design Notes
### SDR data structure
In this project, sparse distributed representations (SDRs) are stored as python sets, aka unordered lists of hashable 
objects. In the base case of representing a bit array, the elements of the set are integers: `'111001' --> SDR({0,1,2,5})`. 
However, because the logic operates on any hashable object, we can use more meaningful data to reference the 'address' of
a given item in the sampled space.  

For example, the temporal memory system handles cell-addressing by using tuples in 
the form `(column_index, cell_index)`.  In _theory_ this could be extended to help split up a single Temporal Memory's 
columns across processes or machines, by including more information in the hashable address: `(ip, port, column, cell)`
or something. Maybe it will work, maybe it won't. It'll be fun to try.

### Object based, rather than matrix based
In order to help me understand the theory, I opted to implement the algorithms in a fairly straightforward, object-oriented
style, as opposed to the matrix-based structure used in other implementations.  That means no `numpy`, `scipy`, etc. 
I imagine this choice will have performance implications, but I still find it a worthwhile option for the time being.

## Major TODO items
### Encoders
- [ ] flexible scalar
- [ ] log (e.g. log(2))
- [ ] delta
- [x] category
- [x] cyclic / ordered (e.g. days of week, hours in day)
- [ ] geospatial data

### General / universal improvements
- [ ] coherent configuration strategy for all levels of code
- [x] use mocking to replace the more intricate tests
- [x] reconcile Spatial Pooler column and TMColumn
- [x] option to disable learning in Spatial Pooler and Temporal Memory
- [ ] integration testing of _any_ kind

### Temporal Pooling
- [x] TemporalMemory 'reset sequence' function

### Documentation
- [ ] Add docstrings to...anything. They are basically all missing.
- [ ] Make a proper quick-start guide to running a sample system
- [ ] Sample systems (gym data, traffic data, etc?)

### Packaging and distribution
- [ ] Flesh out setup.py
- [ ] Tox to build and test multiple python versions
- [ ] pypi and all that jazz

## Key references
- [BAMI](https://numenta.com/resources/biological-and-machine-intelligence/) - the primary source of knowledge for this implementation
- [Nupic](https://github.com/numenta/nupic) - an official implementation of HTM, great for making sure I understood parts of BAMI
- [BBHTM](https://github.com/vsraptor/bbhtm) - for some initial help making sense of Spatial Pooling
- [HTM School](https://numenta.org/htm-school/) - for helpful overviews of the main concepts