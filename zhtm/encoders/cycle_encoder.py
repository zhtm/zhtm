"""
Cyclic category encoder
"""
from zhtm.sdr import SDR


def wrap_bounds(i, min_i, max_i):
    val_range = (max_i + 1) - min_i  # 0-5 == range of 6 values
    if i < min_i:
        delta = min_i - i
        ret_i = (
            i
            + int(delta / val_range) * val_range
            + (val_range if delta % val_range > 0 else 0)
        )
    elif i > max_i:
        delta = i - max_i
        ret_i = (
            i
            - int(delta / val_range) * val_range
            - (val_range if delta % val_range > 0 else 0)
        )
    else:
        ret_i = i
    return ret_i


class CycleEncoder:
    def __init__(self, categories, bits_per_category=10, bit_buffer=0):
        self.categories = categories
        self.bits_per_category = bits_per_category
        self.bit_buffer = bit_buffer
        self.num_bits_active = self.bits_per_category + (2 * bit_buffer)
        self.num_bits = self.bits_per_category * len(categories)

    def encode(self, category):
        category_i = self.categories.index(category)
        category_start = self.bits_per_category * category_i
        bit_start = category_start - self.bit_buffer
        bit_end = category_start + self.bits_per_category + self.bit_buffer

        on_bits = [
            wrap_bounds(i, 0, self.num_bits - 1) for i in range(bit_start, bit_end)
        ]
        return SDR(on_bits)
