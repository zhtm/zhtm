from zhtm.encoders.cycle_encoder import CycleEncoder


class MonthEncoder(CycleEncoder):
    def __init__(self, bits_per_category, bit_buffer):
        categories = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12]  # 1==Jan, 12==Dec
        super(MonthEncoder, self).__init__(categories, bits_per_category, bit_buffer)

    def encode(self, datelike_object):
        category = datelike_object.month
        return super(MonthEncoder, self).encode(category)


class DayOfWeekEncoder(CycleEncoder):
    def __init__(self, bits_per_category, bit_buffer):
        categories = [0, 1, 2, 3, 4, 5, 6]  # 0 == Monday, 6 == Sunday
        super(DayOfWeekEncoder, self).__init__(
            categories, bits_per_category, bit_buffer
        )

    def encode(self, datelike_object):
        category = datelike_object.weekday()
        return super(DayOfWeekEncoder, self).encode(category)


class HourOfDayEncoder(CycleEncoder):
    def __init__(self, bits_per_category, bit_buffer):
        categories = [i for i in range(24)]
        super(HourOfDayEncoder, self).__init__(
            categories, bits_per_category, bit_buffer
        )

    def encode(self, timelike_object):
        category = timelike_object.hour
        return super(HourOfDayEncoder, self).encode(category)


class WeekendEncoder(CycleEncoder):
    def __init__(self, bits_per_category, bit_buffer):
        categories = ["weekday", "weekend"]
        super(WeekendEncoder, self).__init__(categories, bits_per_category, bit_buffer)

    def encode(self, datelike_object):
        category = "weekday" if datelike_object.weekday() <= 4 else "weekend"
        return super(WeekendEncoder, self).encode(category)
