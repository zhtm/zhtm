"""
Merges multiple inputs / encoders into a single SDR.
"""
from zhtm.sdr import SDR


class MultiEncoder:
    def __init__(self, encoders):
        self.encoders = encoders
        self.num_bits = sum(e.num_bits for e in encoders)

    def encode(self, value_list):
        """
        Encode data in value_list, returning SDR representing all inputs
        :param value_list: list of inputs, one per stored encoders
        :return:
        """
        active_indices = []
        current_n = 0

        if len(self.encoders) != len(value_list):
            template = "Number of encoders({}) much match number of inputs({})"
            raise ValueError(template.format(len(self.encoders), len(value_list)))

        for encoder, data in zip(self.encoders, value_list):
            new_indices = [i + current_n for i in encoder.encode(data)]
            active_indices.extend(new_indices)
            current_n += encoder.num_bits
        else:
            assert current_n == self.num_bits

        return SDR(active_indices)
