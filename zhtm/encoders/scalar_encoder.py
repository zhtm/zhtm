"""
Simple scalar encoder.
"""
from zhtm.sdr import SDR


class ScalarEncoder:
    """
    A Simple scalar encoder. Replace this with something more useful when creating a real HTM program.

    1. Choose the range of values: `min_val`, `max_val`
    2. `range = max_val - min_val`
    3. choose number of buckets, `num_buckets`
    4. choose number of active bits per representation, `active_bits`
    5. Compute total number of bits: `num_bits = num_buckets + active_bits - 1`
    6. For a given value, `v`, appropriate bucket: `bucket_i = floor(num_buckets * (value - min_val) / range)`
    7. Create encoded representation: set `active_bits` consecutive bits, starting at index `bucket_i` active
    """

    def __init__(self, min_val=0, max_val=100, num_active_bits=21, num_buckets=100):
        self.min_val = min_val
        self.max_val = max_val
        self.range = max_val - min_val  # 0 --> 10 is 11 'slots'
        self.num_active_bits = num_active_bits
        self.num_buckets = num_buckets
        self.num_bits = num_buckets + num_active_bits

    def __str__(self):
        t = "ScalarEncoder:[{min_val},{max_val}]=>R:{range}|active_bits:{active_bits}|buckets:{buckets}|bits:{bits}"
        return t.format(
            min_val=self.min_val,
            max_val=self.max_val,
            range=self.range,
            active_bits=self.num_active_bits,
            buckets=self.num_buckets,
            bits=self.num_bits,
        )

    def encode(self, value):
        value = min(max(value, self.min_val), self.max_val)
        bucket_i = int(self.num_buckets * (value - self.min_val) / self.range)
        active_indices = range(bucket_i, bucket_i + self.num_active_bits)  # + 1)
        return SDR(active_indices)


if __name__ == "__main__":
    encoder = ScalarEncoder()
    print(encoder)

    for i in range(98, 100):
        print(encoder.encode(i))
