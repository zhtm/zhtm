"""
Spatial Pooling algorithm
"""
import heapq
import logging
import random
from collections import Counter

from zhtm.sdr import SDR
from zhtm.sp_column import SPColumn


class NaiveSpatialPooler:
    """
    Spatial pooler implementation.

    'Naive' in that it is topologically blind; it has no local-inhibition mechanism.
    """

    def __init__(
        self,
        num_input_bits,
        num_columns,
        stimulus_threshold=0,
        winning_column_count=10,
        min_overlap_duty_cycle_percent=0.001,
        boost_strength=0.0,
    ):
        self.num_input_bits = num_input_bits
        self.columns = {
            address_i: SPColumn(address_i) for address_i in range(num_columns)
        }

        self.cycle_count = 0
        self.stimulus_threshold = stimulus_threshold
        self.winning_column_count = winning_column_count
        self.min_overlap_duty_cycle_percent = min_overlap_duty_cycle_percent
        self.boost_strength = boost_strength
        self.do_learning = True

    def randomize_column_synapses(self, num_potential_synapses_per_column):
        for address in self.columns:
            self.columns[address].randomize_potential_synapses(
                num_potential_synapses_per_column, self.num_input_bits
            )

    def send_input_to_columns(self, input_sdr):
        boosted_value_pairs = []
        for address, c in self.columns.items():
            c.process_input(input_sdr)
            boosted_count = c.get_boosted_count()
            boosted_value_pairs.append((boosted_count, address))
        return boosted_value_pairs

    def get_winning_columns(self, boosted_activation_pairs):
        """
        NOTE: this is the 'global inhibition' style of choosing winners.

        No attempt made at this time to do any local / topological inhibition.

        :param boosted_activation_pairs: list of (boosted_overlap_count, column_index) tuples
        :return:
        """
        n_largest = heapq.nlargest(self.winning_column_count, boosted_activation_pairs)
        winning_indices = {x[1] for x in n_largest if x[0] >= self.stimulus_threshold}
        return winning_indices

    def adapt_winning_columns(self, winning_column_indices):
        for column_index in winning_column_indices:
            self.columns[column_index].adjust_permanences()

    def update_column_duty_cycles(self, winning_column_indices):
        max_overlap_duty_cycle = 0.0
        for address, c in self.columns.items():
            c.update_active_duty_cycle(
                self.cycle_count, address in winning_column_indices
            )
            c.update_overlap_duty_cycle(self.cycle_count)
            max_overlap_duty_cycle = max(max_overlap_duty_cycle, c.overlap_duty_cycle)
        return max_overlap_duty_cycle

    def adjust_column_duty_cycle_contributors(self, max_overlap_duty_cycle):
        """
        Adjust individual columns to push them towards ideal active and overlap duty cycles.

        The NaiveRegion is configured to monitor and adjust its columns' duty cycles.

        ## Active Duty Cycle adjustment
        The region will allow `winning_column_count` number of columns to be active for each input,
        which means that `winning_column_count / number of columns` is the percentage of columns
        active for each input. If the input space evenly activates all columns in the region, we
        should then expect that each column is active `winning_column_count / number of columns`
        percentage of the time. This is our ideal `active_duty_cycle`.

        Any column with an `active_duty_cycle` below this value gets its `active_boost_multiplier`
        increased over 1.0, meaning that that column is more likely to 'win' when it receives an
        overlapping input, while any column whose `active_duty_cycle` is above the ideal sees its
        boost multiplier lowered below 1.0, making it less likely to be considered 'active' after
        inhibition. Repeated applications of this adjustment _should_ drive the average active duty
        cycle towards the ideal.

        The degree to which a single column can be boosted is set by `NaiveRegion.boost_strength`,
        which is a sort of learning tuning parameter.  If that value is set to 0.0, then no boosting
        occurs, and we will not actively intervene in columns' active_duty_cycles.

        ## Overlap Duty Cycle Adjustment
        There is no 'ideal' overlap duty cycle, but we do want to make sure that a column overlaps
        often enough that it has a chance to activate. If any columns' overlap duty cycle drops
        below a dynamic threshold (defined as a small percentage of the maximum duty cycle of
        all columns), then we boost _all_ of that column's synapse permanences to give the column
        a chance to connect more synapses.
        """

        desired_activation_duty_cycle = self.winning_column_count / len(self.columns)
        min_overlap_duty_cycle_without_increase = (
            max_overlap_duty_cycle * self.min_overlap_duty_cycle_percent
        )

        for c in self.columns.values():
            c.adjust_boost_multiplier(
                desired_activation_duty_cycle, self.boost_strength
            )

            if c.overlap_duty_cycle < min_overlap_duty_cycle_without_increase:
                c.increase_permanences()

    def process_input(self, input_sdr):
        self.cycle_count += 1

        boosted_activation_pairs = self.send_input_to_columns(input_sdr)

        winning_column_addresses = self.get_winning_columns(boosted_activation_pairs)

        if self.do_learning:
            self.adapt_winning_columns(winning_column_addresses)

        max_overlap_duty_cycle = self.update_column_duty_cycles(
            winning_column_addresses
        )

        if self.do_learning:
            self.adjust_column_duty_cycle_contributors(max_overlap_duty_cycle)

        output_sdr = SDR(winning_column_addresses)

        return output_sdr


class SpatialPoolerHealthCheck:
    """
    Runs a health check on a given Spatial Pooler instance, looking for things which
    violate suggested guidelines or are known to cause behavior problems.

    This class is meant to be a convenience utility that encapsulates a lot of
    'best practices' from the HTM community. Passing these checks won't guarantee that
    your system will work, just as failing these checks won't guarantee failure.

    -----------------------------------------------------------
    # Health checks for a potential SpatialPooler configuration
    * on average, at least 15-20 input bits should overlap with connected synapses?

    -----DONE----
    * verify that output SDR has fixed sparsity
    * warn if output SDR is too dense (Numenta suggests around 2% of total columns)
    * verify that output SDR has minimum 25 active mini-columns (BAMI recommendation)
    * warn if minicolumn count < 2048
    * warn if stimulus_threshold > 5 (BAMI suggests 0-5, with 0 as default)

    * verify that input SDR has 'enough' on bits (40+?)
    * permanence_increment generally larger than permanence_decrement
    * as noise is added to one input, overlap between outputs drops (turn learning off!)
    * identical inputs result in identical outputs
    """

    INPUT_SDR_MINIMUM_SUGGESTED_ON_BITS = 40
    OUTPUT_SDR_MINIMUM_SUGGESTED_ACTIVE_COLUMNS = 25
    COLUMN_COUNT_MIN_SUGGESTED = 2048
    STIMULUS_THRESHOLD_MAX_SUGGESTED = 5

    def __init__(self, spatial_pooler, sample_inputs):
        self.sp = spatial_pooler
        self.sample_inputs = sample_inputs
        self.logger = logging.getLogger("HEALTH")
        self.logger.setLevel(logging.INFO)

        # TODO: replace all of this validation with `cerberus` schemas?

    def _section_header(self, title):
        self.logger.info(title.center(80, "-"))

    @staticmethod
    def fuzz_sdr(sdr, fuzz_percent, num_input_bits):
        fuzz_count = int(num_input_bits * fuzz_percent)
        bits_to_fuzz = set(random.sample(range(num_input_bits), fuzz_count))
        return SDR(sdr.symmetric_difference(bits_to_fuzz))

    def validate(self):
        self.sp.do_learning = False

        self.validate_sample_inputs()
        self.validate_configuration()
        self.validate_output()
        self.validate_output_overlap()
        self.validate_output_consistency()

    def validate_configuration(self):
        self._section_header("Validating Configuration")

        # Stimulus threshold mismatch  TODO: maybe fix this so it can't happen?  Why allow this at all?
        if self.sp.stimulus_threshold != SPColumn.stimulus_threshold:
            msg = "FAIL: SpatialPooler.stimulus_threshold({}) != SPColumn.stimulus_threshold({})"
            self.logger.warning(
                msg.format(self.sp.stimulus_threshold, SPColumn.stimulus_threshold)
            )
        else:
            self.logger.info(
                "PASS: SpatialPooler.stimulus_threshold({}) == SPColumn.stimulus_threshold({})"
            )

        # Minimum column count
        if len(self.sp.columns) < self.COLUMN_COUNT_MIN_SUGGESTED:
            msg = "FAIL: Number of columns({}) below suggested minimum({})"
            self.logger.warning(
                msg.format(len(self.sp.columns), self.COLUMN_COUNT_MIN_SUGGESTED)
            )
        else:
            msg = "PASS: Number of columns above suggested minimum"
            self.logger.info(msg)

        # stimulus max suggested
        # TODO: validate in range [0, 5], instead?
        if self.sp.stimulus_threshold > self.STIMULUS_THRESHOLD_MAX_SUGGESTED:
            msg = "FAIL: stimulus threshold({}) above suggested max({})"
            self.logger.warning(
                msg.format(
                    self.sp.stimulus_threshold, self.STIMULUS_THRESHOLD_MAX_SUGGESTED
                )
            )
        else:
            self.logger.info("PASS: stimulus threshold below maximum")

        # Permanence increment / decrement
        if SPColumn.permanence_increment <= SPColumn.permanence_decrement:
            msg = "FAIL: SPColumn.permanence_increment({}) <= SPColumn.permanence_decrement({})"
            self.logger.warning(
                msg.format(SPColumn.permanence_increment, SPColumn.permanence_decrement)
            )
        else:
            self.logger.info(
                "PASS: SPColumn.permanence_increment > SPColumn.permanence_decrement"
            )

    def validate_output(self):
        self._section_header("Validating Configuration")

        outputs = []
        for input_sdr in self.sample_inputs:
            output_sdr = self.sp.process_input(input_sdr)
            outputs.append(output_sdr)

        # Check for fixed sparsity
        output_sparsities = [len(x) for x in outputs]
        if len(set(output_sparsities)) > 1:
            self.logger.warning(
                "FAIL: Output SDRs have different sparsities: {}".format(
                    output_sparsities
                )
            )
        else:
            self.logger.info("PASS: Output SDRs have same sparsities")

        # Check for sparsity level
        desired_sparsity = 0.02
        actual_sparsity = output_sparsities[0] / len(self.sp.columns)
        acceptable_delta = 0.01
        args = round(actual_sparsity * 100, 2), round(desired_sparsity * 100, 2)
        if abs(desired_sparsity - actual_sparsity) >= acceptable_delta:
            msg = "FAIL: Actual sparsity({}%) not close enough to suggested({}%)"
            self.logger.warning(msg.format(*args))
        else:
            msg = "PASS: Actual sparsity({}%) close to suggested({}%)"
            self.logger.info(msg.format(*args))

        # Check for minimum active columns in output
        if output_sparsities[0] < self.OUTPUT_SDR_MINIMUM_SUGGESTED_ACTIVE_COLUMNS:
            msg = "FAIL: output sdr contains too few active columns ({} < {})"
            self.logger.warning(
                msg.format(
                    output_sparsities[0],
                    self.OUTPUT_SDR_MINIMUM_SUGGESTED_ACTIVE_COLUMNS,
                )
            )
        else:
            msg = "PASS: output sdr has more active columns than minimum suggested"
            self.logger.info(msg)

    def validate_sample_inputs(self):
        self._section_header("Validating Sample inputs")
        for i, input_sdr in enumerate(self.sample_inputs):
            num_on_bits_exceeds_minimum = (
                len(input_sdr) > self.INPUT_SDR_MINIMUM_SUGGESTED_ON_BITS
            )

            if num_on_bits_exceeds_minimum:
                self.logger.info("PASS: Input {}: count of active bits".format(i))
            else:
                self.logger.warning("FAIL: Input {}: count of active bits".format(i))

    def validate_output_overlap(self):
        self._section_header("Validating Input-->Output overlap similarity")
        num_bits = self.sp.num_input_bits
        base_input = self.sample_inputs[0]

        attempts = 10
        attempts_with_complete_divergence = 0

        for _ in range(attempts):
            overlap_scores = []
            percent_list = (x / 100 for x in (0, 80, 5))
            for percent in percent_list:
                fuzzed_input = self.fuzz_sdr(base_input, percent, num_bits)

                base_output = self.sp.process_input(base_input)
                fuzzed_output = self.sp.process_input(fuzzed_input)

                overlap = base_output.overlap(fuzzed_output)
                overlap_scores.append(overlap)

                attempts_with_complete_divergence += (
                    1 if sorted(overlap_scores, reverse=True) == overlap_scores else 0
                )

        percent_total_convergence = attempts_with_complete_divergence / attempts
        if percent_total_convergence > 0.75:
            self.logger.info("PASS: fuzzing inputs causes outputs to diverge")
        else:
            msg = "FAIL: fuzzing inputs did not cause outputs to diverge often enough ({}%)"
            self.logger.warning(msg.format(percent_total_convergence * 100))

    def validate_output_consistency(self):
        self._section_header("Validate output consistency")

        counter_list = [Counter() for _ in range(len(self.sample_inputs))]

        for _ in range(10):
            for i, input_sdr in enumerate(self.sample_inputs):
                output_sdr = self.sp.process_input(input_sdr)

                counter_list[i].update([tuple(sorted(list(output_sdr)))])

        for i in range(len(self.sample_inputs)):
            num_distinct_outputs = len(counter_list[i])
            if num_distinct_outputs > 1:
                msg = "FAIL: Input {}: Number of distinct outputs({}) != 1"
                self.logger.warning(msg.format(i, num_distinct_outputs))
            else:
                msg = "PASS: Input {}: Number of distinct outputs == 1"
                self.logger.info(msg.format(i))
