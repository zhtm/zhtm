"""
Home for utility functions.
"""


def clamp(x, min_value, max_value):
    if x < max_value:
        if x < min_value:
            return min_value
        else:
            return x
    else:
        return max_value
