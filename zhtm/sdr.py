"""
SDR and stuff.
"""

import random


class SDR(set):
    """
    Sparse-distributed representation class.
    """

    def as_binary_string(self, num_bits_to_display):
        """
        Return a string of 1 and 0 representing the SDR.

        NOTE: this function, like `random()` below, assumes that the SDR address spaces
        is made of integers. This assumption may not hold in the future, when addressing
        cells in the Temporal Memory system.

        TODO: move to subclass? (IntegerSDR or something?)

        :param num_bits_to_display: number of integers in the entire address space
        :return:
        """
        return "".join(["1" if i in self else "0" for i in range(num_bits_to_display)])

    def __and__(self, other):
        return SDR(set.__and__(self, other))

    def __or__(self, other):
        return SDR(set.__or__(self, other))

    def __sub__(self, other):
        return SDR(set.__sub__(self, other))

    def overlap(self, other):
        return len(self & other)

    def match(self, other, threshold=1):
        return self.overlap(other) >= threshold

    @classmethod
    def random(cls, num_bits=100, num_ones=20):
        """
        Return a random SDR that samples the input space defined by (0,num_bits]

        This method assumes that each 'address' in the sample space is just an
        integer. In the future, we may introduce more complex SDR addresses,
        e.g. (column_index=x, cell_index=y). The _logic_ of manipulating such
        addresses should be unchanged, though functions like this one, which assume
        an integer address space, will need to be abstacted.
        :param num_bits: number of addresses in the space
        :param num_ones: number of addresses to be ON
        :return:
        """

        return SDR(random.sample(range(num_bits), num_ones))

    # def union(self, other):
    #     pass
    #     # return SDR(self | other)
