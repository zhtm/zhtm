import random

from zhtm.sdr import SDR
from zhtm.utilities import clamp


class Synapse:
    """
    Represents a connection between neurons.
    """

    def __init__(self, address, permanence=0.01):
        """
        :param address: address this synapse connects to. Represents either part of the input space
            (for proximal synapses) or a cell address (for distal synapses)
        :param permanence:  starting permanence value of this synapse.
        """
        self.address = address
        self._permanence = 0.0
        self.permanence = permanence

    @property
    def permanence(self):
        return self._permanence

    @permanence.setter
    def permanence(self, new_value):
        """
        Setter for `self.permanence_value` to constrain value to [0.0, 1.0]
        :param new_value:
        :return:
        """
        self._permanence = clamp(new_value, 0.0, 1.0)


class DendriteSegment:
    """
    TODO: document DendriteSegment

    Class-level variables used for configuration values.
    """

    connected_permanence = 0.5
    connected_permanence_standard_deviation = 0.005

    permanence_increment = 0.01
    permanence_decrement = 0.01
    predicted_decrement = 0.001

    initial_distal_permanence = 0.21

    def __init__(self):
        self.potential_synapses = dict()  # Yes, this should be a dict.
        self._potential_sdr = SDR(
            set()
        )  # Subset of input space -- has setter and getter
        self.potential_sdr = SDR(set())  # Set so PyCharm believes this is a valid value
        self.connected_sdr = SDR(set())  # Subset of `potential_sdr`
        self.active_sdr = SDR(set())  # Subset of `potential_sdr`

        self.active_connected_sdr = SDR(set())  # `active_sdr` intersect `connected_sdr`

    @property
    def potential_sdr(self):
        """
        Property for `_potential_sdr`.

        We use a property for this value because we want `potential_synapses` and
        `connected_sdr` to be updated whenever `potential_sdr` is set.
        """
        return self._potential_sdr

    @potential_sdr.setter
    def potential_sdr(self, potential_synapse_sdr):
        """
        Setter for `self.potential_sdr`.

        Whenever `self.potential_sdr` is set to a new SDR, we randomize a matching set
        of synapses, as well as calculate `self.connected_sdr`. Note that if
        `self.potential_synapses` already contains Synapses, only newly-added Synapses
        will be randomized; existing Synapses will remain untouched.

        TODO: make sure this works for both proximal and distal segments, as they have different usage patterns

        :param potential_synapse_sdr:
        :return:
        """
        self._potential_sdr = potential_synapse_sdr
        self.potential_synapses.update(
            {
                address: Synapse(
                    address,
                    clamp(
                        random.gauss(  # random permanence
                            self.connected_permanence,
                            self.connected_permanence_standard_deviation,
                        ),
                        0.0,  # Min permanence value
                        1.0,  # Max permanence value
                    ),
                )
                for address in self.potential_sdr
                if address not in self.potential_synapses
            }
        )
        self._update_connected_sdr()

    def _update_connected_sdr(self):
        """
        Create `self.connected_sdr` based on the connected Synapses in `self.potential_synapses`
        :return:
        """
        self.connected_sdr = SDR(
            {
                address
                for address, synapse in self.potential_synapses.items()
                if synapse.permanence > self.connected_permanence
            }
        )

    def randomize_potential_synapses(self, num_potential_synapses, address_space):
        """
        Randomly initialize potential synapses for this dendrite segment.

        A random subset of the address space (aka a random SDR) is created to represent
        the _potential_ synapses that this column is capable of forming. Each potential
        synapse is then given a normally distributed `permanence` value, centered around
        the minimum value for synapse to be considered 'connected' to the column.

        Given the normal distribution, we expect _roughly_ 50% of the potential synapses
        to be 'connected' after randomization.

        This is primarily intended for use with proximal segments. Distal synapses are
        created non-randomly, with a constant initial `permanence` value.

        :param num_potential_synapses:
        :param address_space:
        :return:
        """
        # Get a random subset of the input space as our potential synapse locations
        self.potential_sdr = SDR.random(address_space, num_potential_synapses)

    def process_input(self, input_sdr):
        """
        Process an `input_sdr`, updating active and active+connected SDRs.

        The `active` SDR represents synapses in the potential pool that overlap with
        the `input_sdr`.

        The `active_connected_sdr` represents synapses in the potential pool that both
        overlap with the `input_sdr` *and* have a high enough permanence to be considered
        'connected'.

        We use `active` for checking whether this segment 'matches' a given input, and
        `active_connected_sdr` for checking whether this segment is activated by a given input.

        :param input_sdr:
        :return:
        """
        self.active_sdr = self.potential_sdr & input_sdr
        self.active_connected_sdr = self.active_sdr & self.connected_sdr

    def is_active(self, activation_threshold):
        """
        Check if the number of `active`, `connected` synapses is >= a threshold.
        :param activation_threshold:
        :return:
        """
        return len(self.active_connected_sdr) >= activation_threshold

    def is_matching(self, matching_threshold):
        """
        Check if the number of active (but not necessarily connected) synapses is >= a threshold.
        :param matching_threshold:
        :return:
        """
        return len(self.active_sdr) >= matching_threshold

    def adjust_active_permanences(self, permanence_delta):
        for address in self.active_sdr:
            self.potential_synapses[address].permanence += permanence_delta

    def adjust_inactive_permanences(self, permanence_delta):
        for address in self.potential_sdr - self.active_sdr:
            # TODO: consider pre-calculating `self.inactive_sdr`?
            self.potential_synapses[address].permanence += permanence_delta

    def adapt_permanences(self, is_correctly_active, comparison_sdr):
        """
        Update the permanence values of all synapses on this segment.

        If this segment was correctly active:
            If a synapse connected to a previously active cell, increase the permanence.
            All other synapses are decreased.

        If this segment was _not_ supposed to be active:
            Decrease the permanence of any active synapse.
        :param is_correctly_active:
        :param comparison_sdr:
        :return:
        """
        if is_correctly_active:
            valid_sdr = self.active_sdr & comparison_sdr
            invalid_sdr = self.potential_sdr - valid_sdr

            # Increase `active` synapse permanences
            for correct_address in valid_sdr:
                self.potential_synapses[
                    correct_address
                ].permanence_value += self.permanence_increment
            # Decrease `inactive` synapse permanences
            for incorrect_address in invalid_sdr:
                self.potential_synapses[
                    incorrect_address
                ].permanence_value -= self.permanence_decrement
        else:
            incorrectly_active_sdr = self.active_sdr & comparison_sdr
            # Decrease `active` synapse permanences
            for address in incorrectly_active_sdr:
                self.potential_synapses[
                    address
                ].permanence_value -= self.predicted_decrement

        self._update_connected_sdr()

    def grow_new_synapses(self, winning_address_sdr, desired_synapse_count):
        """
        # TODO: document this
        :param winning_address_sdr:
        :param desired_synapse_count:
        :return:
        """
        # We should never have _more_ synapses than `desired_synapse_count`,
        # but if we do, this may do unwanted things.
        new_synapse_count = desired_synapse_count - len(self.potential_synapses)

        # Exclude existing synapses from choices for new synapses
        valid_options = winning_address_sdr - self.potential_sdr

        new_synapse_count = min(new_synapse_count, len(valid_options))
        new_addresses = random.sample(valid_options, new_synapse_count)

        new_potential_sdr = self.potential_sdr | SDR(new_addresses)
        self.potential_sdr = new_potential_sdr

        # TODO: synapses have random permanence due to potential_sdr property. Wasted effort?
        for i in new_addresses:
            self.potential_synapses[i].permanence_value = self.initial_distal_permanence

        # TODO: if we didn't have to reset the permanences post `potential_sdr` property, this would already be done
        # We updated permanence values, so make sure to update `connected_sdr` as well.
        self._update_connected_sdr()
