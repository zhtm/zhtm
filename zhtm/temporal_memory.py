"""
Implementation of Temporal Memory algorithm.

TODO: make spatial pooler and temporal memory use same DendriteSegment class
"""
import random

from zhtm.sdr import SDR
from zhtm.utilities import clamp


class TemporalMemory:
    def __init__(self, column_addresses):

        self.columns = {address: TMColumn(address) for address in column_addresses}
        self._col_predicted_sdr = SDR()  # set during 'predict()'

        # Following are set during `_set_column_buckets`
        self._col_active_predicted_sdr = SDR()
        self._col_active_unpredicted_sdr = SDR()
        self._col_inactive_predicted_sdr = SDR()

        # Store 'active' and 'winner' cells for use in predicting next time step
        self._stored_active_cell_sdr = SDR()  # Used when adjusting synapses
        self._stored_winner_cell_sdr = SDR()  # Used when growing new synapses

    def _set_column_buckets(self, spatial_pooler_column_sdr):
        self._col_active_predicted_sdr = spatial_pooler_column_sdr.intersection(
            self._col_predicted_sdr
        )
        self._col_active_unpredicted_sdr = (
            spatial_pooler_column_sdr - self._col_predicted_sdr
        )
        self._col_inactive_predicted_sdr = (
            self._col_predicted_sdr - spatial_pooler_column_sdr
        )

    def _handle_active_predicted_columns(self):
        """
        :return:
        """
        active_cell_sdr = SDR()
        winner_cell_sdr = SDR()
        for column_address in self._col_active_predicted_sdr:
            col = self.columns[column_address]
            col_active_cell_sdr = col.handle_active_and_predicted(
                self._stored_active_cell_sdr, self._stored_winner_cell_sdr
            )

            # In this case, `active` and `winner` cells are identical
            active_cell_sdr.update(col_active_cell_sdr)
            winner_cell_sdr.update(col_active_cell_sdr)

        return active_cell_sdr, winner_cell_sdr

    def _handle_active_unpredicted_columns(self):
        """
        :return:
        """
        active_cell_sdr = SDR()
        winner_cell_sdr = SDR()
        for column_address in self._col_active_unpredicted_sdr:
            col = self.columns[column_address]
            col_active_cell_sdr, col_winner_cell_sdr = col.handle_active_and_unpredicted(
                self._stored_active_cell_sdr, self._stored_winner_cell_sdr
            )
            # print("Bursted col {}:\t winner cell: {}".format(column_address, col_winner_cell_sdr))
            active_cell_sdr.update(col_active_cell_sdr)
            winner_cell_sdr.update(col_winner_cell_sdr)
        return active_cell_sdr, winner_cell_sdr

    def _handle_inactive_predicted_columns(self):
        """
        :return:
        """
        if TMCell.learning_enabled:
            for column_address in self._col_inactive_predicted_sdr:
                self.columns[column_address].handle_inactive_and_predicted(
                    self._stored_active_cell_sdr
                )

    def _predict_next_input(self, active_cell_sdr):
        """
        :return:
        """
        predicted_column_sdr = SDR()
        for address in self.columns:
            # Expect return value to be either:
            #   * `SDR()` if column is not predicted
            #   * `SDR([column.address])` if column is predicted
            col_sdr = self.columns[address].predict_next_input(active_cell_sdr)
            predicted_column_sdr.update(col_sdr)
        self._col_predicted_sdr = predicted_column_sdr

    def process_input(self, spatial_pooler_column_sdr):
        self._set_column_buckets(spatial_pooler_column_sdr)

        active_cell_sdr = SDR()  # Active cell addresses for this input
        winner_cell_sdr = SDR()  # Winner cell addresses for this input

        # Handle correctly predictive columns
        active, winner = self._handle_active_predicted_columns()
        active_cell_sdr.update(active)
        winner_cell_sdr.update(winner)

        # Handle columns which _should_ have been predictive
        active, winner = self._handle_active_unpredicted_columns()
        active_cell_sdr.update(active)
        winner_cell_sdr.update(winner)

        # Handle columns which were incorrectly predictive
        self._handle_inactive_predicted_columns()

        # Use current set of active cells to predict next input
        self._predict_next_input(active_cell_sdr)

        # All learning / growing is complete for this stage, so
        # we can store the current active / winner cells for use
        # in the next call to `process_input()`
        self._stored_active_cell_sdr = active_cell_sdr
        self._stored_winner_cell_sdr = winner_cell_sdr

    def reset_pattern(self):
        self._col_predicted_sdr = SDR()
        self._stored_active_cell_sdr = SDR()
        self._stored_winner_cell_sdr = SDR()

        for address in self.columns:
            self.columns[address].reset_pattern()


class TMColumn:
    num_cells = 32

    def __init__(self, address):
        self.address = address
        self.cells = dict()

        self.active_cell_sdr = SDR()

        for i in range(self.num_cells):
            cell_address = (self.address, i)
            self.cells[cell_address] = TMCell(cell_address)

    def handle_active_and_predicted(self, previous_active_cells, previous_winner_cells):
        """
        :param previous_active_cells:
        :param previous_winner_cells:
        :return:
        """
        active_cell_sdr = SDR()
        for cell in self.cells.values():
            this_cell_active = cell.handle_active_and_predicted(
                previous_active_cells, previous_winner_cells
            )
            if this_cell_active:
                active_cell_sdr.update([cell.address])

        # We only need to return the one SDR, as it is used for both active and winner cells
        return active_cell_sdr

    def handle_active_and_unpredicted(
        self, previous_active_cells, previous_winner_cells
    ):
        """
        :return: (active_cell_sdr, winner_cell_sdr)
        """
        active_cell_sdr = SDR(self.cells.keys())
        winning_cell_sdr, learning_segment = self._get_bursted_cell_and_segment()

        if TMCell.learning_enabled and learning_segment is not None:
            learning_segment.learn(previous_active_cells, previous_winner_cells)
        return active_cell_sdr, winning_cell_sdr

    def handle_inactive_and_predicted(self, previous_active_cell_addresses):
        """
        :return:
        """
        for cell in self.cells.values():
            cell.punish_matching_segments(previous_active_cell_addresses)

    def predict_next_input(self, active_cell_sdr):
        """
        :param active_cell_sdr: Complete SDR of all active cells in TM.
        :return: SDR([self.address]) if there is a predictive cell, else SDR()
        """
        predicted_cell_sdr = SDR()
        for cell_address, cell in self.cells.items():
            # Expect `SDR([cell.address])` if cell is active, `SDR()` otherwise
            predicted_cell_sdr.update(cell.predict_next_input(active_cell_sdr))
        self.active_cell_sdr = predicted_cell_sdr

        return SDR([self.address]) if len(self.active_cell_sdr) > 0 else SDR()

    def reset_pattern(self):
        self.active_cell_sdr = SDR()

    def _get_bursted_cell_and_segment(self):
        """
        :return: (winning_cell_sdr, learning_segment)
        """
        best_score = 0
        best_segment = None
        winning_cell_address = None

        # If we have a cell with more segments, something is very wrong.
        lowest_segment_count = 10000000000
        least_used_cells = []

        # Find best 'matching' segment, if any exist
        # At the same time, keep track of which cells have the fewest segments.
        for cell in self.cells.values():
            cur_best_score, cur_best_segment = cell.get_best_matching_segment()

            if cur_best_score is not None and cur_best_score > best_score:
                best_score = cur_best_score
                best_segment = cur_best_segment
                winning_cell_address = cell.address

            # Keep track of cells with fewest segments, in case no
            # 'best matching' segment is found
            cur_segment_count = len(cell.distal_segments)
            if cur_segment_count < lowest_segment_count:
                lowest_segment_count = cur_segment_count
                least_used_cells = [cell.address]
            elif cur_segment_count == lowest_segment_count:
                least_used_cells.append(cell.address)

        learning_segment = best_segment
        # If needed, find least used cell
        if learning_segment is None:
            winning_cell_address = random.choice(least_used_cells)
            if TMCell.learning_enabled:
                learning_segment = self.cells[winning_cell_address].grow_new_segment()

        return SDR([winning_cell_address]), learning_segment


class TMCell:
    learning_enabled = True
    distal_activation_threshold = 15
    distal_matching_threshold = 10

    def __init__(self, address):
        self.address = address
        self.distal_segments = dict()

    def handle_active_and_predicted(self, previous_active_cells, previous_winner_cells):
        """
        :param previous_active_cells:
        :param previous_winner_cells:
        :return:
        """
        has_active_segment = False
        for segment in self.distal_segments.values():
            if segment.is_active:
                has_active_segment = True
                # NOTE: potential performance optimization: if learning disabled, exit loop after first active segment
                if TMCell.learning_enabled:
                    segment.learn(previous_active_cells, previous_winner_cells)

        return has_active_segment

    def predict_next_input(self, active_cell_sdr):
        """
        :param active_cell_sdr:
        :return: SDR([self.address]) if this cell has an active segment, or SDR() otherwise
        """
        has_active_segment = False
        for segment in self.distal_segments.values():
            this_segment_active = segment.handle_input(
                active_cell_sdr,
                self.distal_activation_threshold,
                self.distal_matching_threshold,
            )
            has_active_segment = has_active_segment or this_segment_active

        if has_active_segment:
            return SDR([self.address])
        else:
            return SDR()

    def get_best_matching_segment(self):
        best_score = 0
        best_segment = None
        for segment in self.distal_segments.values():
            cur_score = segment.num_active_potential_synapses
            if segment.is_matching and cur_score > best_score:
                best_score = cur_score
                best_segment = segment
        return best_score, best_segment

    def grow_new_segment(self):
        if len(self.distal_segments) < 128:
            new_address = (self.address, len(self.distal_segments))
            self.distal_segments[new_address] = TMDendriteSegment(new_address)
            return self.distal_segments[new_address]
        else:
            return None

    def punish_matching_segments(self, previous_active_cell_addresses):
        for segment in self.distal_segments.values():
            if segment.is_matching:
                segment.punish(previous_active_cell_addresses)


class TMDendriteSegment:
    permanence_increment = 0.01  # TODO: sensible default?
    permanence_decrement = 0.005  # TODO: sensible default?

    # From NuPIC:
    #     A good value is just a bit larger than
    #     (the column-level sparsity * permanenceIncrement). So, if column-level
    #     sparsity is 2% and permanenceIncrement is 0.01, this parameter should be
    #     something like 4% * 0.01 = 0.0004).
    predicted_decrement = 0.0004  # TODO: sensible default

    default_permanence = 0.21
    connected_permanence = 0.5
    new_synapse_sample_size = 20  # 41  # Num new synapses to grow
    max_synapses_per_segment = 64

    def __init__(self, address):
        self.address = address

        self._potential_permanences = dict()  # {address: permanences}
        self._potential_sdr = SDR()  # SDR of 'potential' addresses
        self._connected_sdr = SDR()  # subset of _potential_sdr

        self.is_active = False
        self.is_matching = False

        # Store after each input for use in next input's `grow_new_synapses()`
        self.num_active_potential_synapses = 0

    def handle_input(self, input_sdr, activation_threshold, matching_threshold):
        """
        :param input_sdr:
        :param activation_threshold: # of connected synapses that overlap with input must be greater than this for segment to 'activate'
        :param matching_threshold: # of potential synapses that overlap with input must be greater than this for segment to 'match'
        :return:
        """
        active_connected_sdr = input_sdr & self._connected_sdr
        matching_sdr = input_sdr & self._potential_sdr

        # Store number of potential synapses that overlap with `input_sdr`
        # for potential use in `grow_synapse()` during next input.
        self.num_active_potential_synapses = len(matching_sdr)

        self.is_active = len(active_connected_sdr) > activation_threshold
        self.is_matching = len(matching_sdr) > matching_threshold

        return self.is_active

    def learn(self, previous_active_cells, previous_winner_cells):
        self._update_permanences(previous_active_cells)
        self._grow_new_synapses(previous_winner_cells)
        self._reconcile_sdrs()

    def _update_permanences(self, previous_active_cells):
        for syn_addr, cur_perm in self._potential_permanences.items():
            if syn_addr in previous_active_cells:
                new_permanence = clamp(cur_perm + self.permanence_increment, 0, 1)
            else:
                new_permanence = clamp(cur_perm - self.permanence_decrement, 0, 1)
            self._potential_permanences[syn_addr] = new_permanence

    def _grow_new_synapses(self, previous_winner_cells):
        """
        :param previous_winner_cells:
        :return:
        """
        candidates = list(previous_winner_cells - self._potential_sdr)
        random.shuffle(candidates)
        new_synapse_count = self._prune_synapses_for_growth(
            len(candidates), previous_winner_cells
        )

        addresses_to_add = []
        num_added = 0
        for candidate in candidates:
            if num_added >= new_synapse_count:
                break

            if candidate not in self._potential_permanences:
                addresses_to_add.append(candidate)
                num_added += 1

        self.add_synapses(SDR(addresses_to_add))

    def _prune_synapses_for_growth(self, num_candidates, previous_winner_cells):
        new_synapse_count = min(num_candidates, self.new_synapse_sample_size)

        # Check if we overrun max allowed synapses
        overrun = (
                len(self._potential_permanences)
                + new_synapse_count
                - self.max_synapses_per_segment
        )
        if overrun > 0:
            # Try to destroy synapses with lowest permanences to make room
            self.destroy_min_permanence_synapses(overrun, previous_winner_cells)

            # Re-calculate new_synapse_count, in case that we can not delete enough
            # synapses to make room for all expected.
            new_synapse_count = min(
                self.max_synapses_per_segment - len(self._potential_permanences),
                new_synapse_count,
            )
        return new_synapse_count

    def add_synapses(self, new_synapse_sdr):
        for presynaptic_address in new_synapse_sdr:
            if presynaptic_address in self._potential_permanences:
                msg = "Can not add synapse; it already exists.  Segment: {}, new synapse: {}"
                raise ValueError(msg.format(self.address, presynaptic_address))
            self._potential_permanences[presynaptic_address] = self.default_permanence

        self._potential_sdr = SDR(self._potential_permanences.keys())

    def destroy_min_permanence_synapses(self, num_to_destroy, exclude_addresses):
        delete_candidates = [
            (perm, addr)
            for addr, perm in self._potential_permanences.items()
            if addr not in exclude_addresses
        ]
        delete_addresses = sorted(delete_candidates)[:num_to_destroy]
        for _, addr in delete_addresses:
            self._potential_permanences[addr] = 0
        self._reconcile_sdrs()

    def _reconcile_sdrs(self):
        # Figure out if we need to delete any synapses with permanence == 0
        keys_to_delete = []
        for k, v in self._potential_permanences.items():
            if v == 0:
                keys_to_delete.append(k)

        # Delete the keys we found
        for key in keys_to_delete:
            del (self._potential_permanences[key])

        # Update both the potential SDR and the connected SDR based on current permanences
        self._potential_sdr = SDR(list(self._potential_permanences.keys()))
        self._connected_sdr = SDR(
            [
                addr
                for addr, perm in self._potential_permanences.items()
                if perm > self.connected_permanence
            ]
        )

    def punish(self, previous_active_cell_sdr):
        """
        :param previous_active_cell_sdr:
        :return:
        """
        for presynaptic_address, cur_perm in self._potential_permanences.items():
            if presynaptic_address in previous_active_cell_sdr:
                new_perm = clamp(cur_perm - self.predicted_decrement, 0, 1)
                self._potential_permanences[presynaptic_address] = new_perm
        self._reconcile_sdrs()
