import math

from zhtm.dendrite_segment import DendriteSegment


class SPColumn:
    """
    Column objects for Spatial Pooler algorithm.

    These columns exist to:
        * have a proximal dendrite segment
        * track active- and overlap-duty cycle
        * manage the synapse permanences of the proximal dendrite segment.
    """

    permanence_increment = 0.01
    permanence_decrement = 0.005
    stimulus_threshold = 0
    overlap_duty_cycle_low_increment = 0.001

    def __init__(self, column_address):
        self.address = column_address
        self.proximal_segment = DendriteSegment()
        self.boost_multiplier = 1.0

        self.active_duty_cycle = 0.0
        self.overlap_duty_cycle = 0.0

    def randomize_potential_synapses(self, num_synapses, synapse_address_space):
        self.proximal_segment.randomize_potential_synapses(
            num_synapses, synapse_address_space
        )

    def process_input(self, input_sdr):
        self.proximal_segment.process_input(input_sdr)

    def get_boosted_count(self):
        """
        Return number of active, connected synapses multiplied by boot_multiplier
        :return:
        """
        return len(self.proximal_segment.active_connected_sdr) * self.boost_multiplier

    def adjust_permanences(self):
        """
        Learn by increasing 'active' synapses and decreasing 'inactive' synapses.
        :return:
        """
        self.proximal_segment.adjust_active_permanences(self.permanence_increment)
        self.proximal_segment.adjust_inactive_permanences(-self.permanence_decrement)

    def update_active_duty_cycle(self, cycle_count, column_is_active):
        """
        Update active_duty_cycle running average.

        Active duty cycle is the percent of inputs during which this column was
        considered 'active' *after* inhibition.
        :param column_is_active:
        :return:
        """
        effective_cycles = min(1000, cycle_count)
        self.active_duty_cycle = (
            ((effective_cycles - 1) * self.active_duty_cycle) + int(column_is_active)
        ) / effective_cycles

    def update_overlap_duty_cycle(self, cycle_count):
        """
        Update overlap_duty_cycle running average.

        Overlap duty cycle is the percentage of inputs during which the column had
        greater than `stimulus_threshold` connected synapses overlapping with
        ON bits in the input SDR.

        :param cycle_count:
        :return:
        """
        effective_cycles = min(1000, cycle_count)
        is_overlapping = self.proximal_segment.is_active(self.stimulus_threshold)
        numerator = ((effective_cycles - 1) * self.overlap_duty_cycle) + int(
            is_overlapping
        )
        self.overlap_duty_cycle = numerator / effective_cycles

    def increase_all_permanences(self):
        self.proximal_segment.adjust_active_permanences(
            self.overlap_duty_cycle_low_increment
        )
        self.proximal_segment.adjust_inactive_permanences(
            self.overlap_duty_cycle_low_increment
        )
        # Worth adding an 'adjust_all_permanences' function?

    def adjust_boost_multiplier(self, desired_active_duty_cycle, boost_strength):
        activation_duty_delta = desired_active_duty_cycle - self.active_duty_cycle
        self.boost_multiplier = math.e ** (activation_duty_delta * boost_strength)
