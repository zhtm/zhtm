import csv
from datetime import datetime
import logging
import os

from zhtm.encoders.scalar_encoder import ScalarEncoder
from zhtm.encoders.datetime_encoders import HourOfDayEncoder
from zhtm.encoders.multi_encoder import MultiEncoder
from zhtm.spatial_pooler import NaiveSpatialPooler

from zhtm.dendrite_segment import DendriteSegment

from zhtm.temporal_memory import (
    TemporalMemory,
    TMCell,
    TMColumn,
    TMDendriteSegment,
)

logger = logging.getLogger(__name__)
"""
HTM system suggested parameters
* Num Columns (N): 2048
* Num Cells per Column (M): 32
* Num of active bits (w) : 41
* Sparsity (w/N) : 2%
* Dendritic Segment Activation Threshold (θ): 15
* Initial Synaptic Permanence: 0.21
* Connection Threshold for Synaptic Permanence: 0.5
* Synaptic Permanence Increment and Decrement: +/- 0.1
* Synaptic Permanence Decrement for Predicted Inactive Segments: 0.01
* Maximum Number of Segments per Cell: 128
* Maximum Number of Synapses per Segment: 128
* Maximum Number of New Synapses Added at each Step: 32

SP
"boostStrength": 0.0,
"numActiveColumnsPerInhArea": 40,
"synPermActiveInc": 0.003,
"synPermConnected": 0.2,
"synPermInactiveDec": 0.0005

TM
"activationThreshold": 13,
"cellsPerColumn": 32,
"columnCount": 2048,
"maxSegmentsPerCell": 128,
"maxSynapsesPerSegment": 32,
"minThreshold": 10,
"newSynapseCount": 20,
"permanenceDec": 0.1,
"permanenceInc": 0.1,

"""


def get_encoder(min_val, max_val, min_buckets=450, max_buckets=450, num_active_bits=502):
    """
    rangePadding = abs(self.inputMax - self.inputMin) * 0.2
    minVal=self.inputMin-rangePadding,
    maxVal=self.inputMax+rangePadding,
    minResolution=0.001,
    :return:
    """
    range_padding = abs(max_val - min_val) * 0.2
    min_val, max_val = min_val - range_padding, max_val + range_padding
    if min_val == max_val:
        min_val -= 50
        max_val += 50
    num_buckets = min(max_buckets, max(min_buckets, int(1 * (max_val - min_val))))

    print("Encoder: ({},{}), buckets: {}".format(min_val, max_val, num_buckets))
    s = ScalarEncoder(
        min_val=min_val,  # 0,
        max_val=max_val,  # 100,
        num_active_bits=num_active_bits,  # 81,  # int(num_buckets * .1),
        num_buckets=num_buckets,
    )
    return s


def get_multi_encoder(min_val, max_val, hour_width=1, hour_radius=1):
    value_encoder = get_encoder(min_val, max_val)
    time_of_day = HourOfDayEncoder(hour_width, hour_radius)
    multi = MultiEncoder([value_encoder, time_of_day])
    return multi


def get_spatial_pooler(
        num_input_bits,
        num_columns=2048,
        stimulus_threshold=0,
        winning_column_count=257,
        min_overlap_duty_cycle_percent=0.001,
        boost_strength=0.00,
        percent_potential=0.05,
        permanence_increment=0.003,
        permanence_decrement=0.0005
):
    """
    :param num_input_bits:
    :param num_columns:
    :param stimulus_threshold:
    :param winning_column_count:
    :param min_overlap_duty_cycle_percent:
    :param boost_strength:
    :param percent_potential:
    :param permanence_increment:
    :param permanence_decrement:
    :return:
    """
    """
       Defaults from:
       https://github.com/numenta/nupic/blob/master/src/nupic/frameworks/opf/common_models/anomaly_params_random_encoder/best_single_metric_anomaly_params_cpp.json

       "spParams": {
           "spatialImp": "cpp",
           "potentialPct": 0.8,
           "columnCount": 2048,
           "globalInhibition": 1,
           "inputWidth": 0,
           "boostStrength": 0.0,
           "numActiveColumnsPerInhArea": 40,
           "seed": 1956,
           "spVerbosity": 0,
           "spatialImp": "cpp",
           "synPermActiveInc": 0.003,
           "synPermConnected": 0.2,
           "synPermInactiveDec": 0.0005
       }
   """
    DendriteSegment.permanence_increment = permanence_increment
    DendriteSegment.permanence_decrement = permanence_decrement

    sp = NaiveSpatialPooler(
        num_input_bits,
        num_columns,
        stimulus_threshold,
        winning_column_count,
        min_overlap_duty_cycle_percent,
        boost_strength,
    )
    sp.randomize_column_synapses(int(num_input_bits * percent_potential))
    return sp


def get_nova_temporal_memory(
        num_columns,
        new_synapse_sample_size=256,
        max_synapses_per_segment=2048,
        permanence_increment=0.05,
        permanence_decrement=0.005,
        predicted_decrement=0.00,
        default_permanence=0.21,
        connected_permanence=0.50,
        learning_enabled=True,
        distal_activation_threshold=32,
        distal_matching_threshold=8,
        num_cells=4
    ):
    """

    :param num_columns:
    :param new_synapse_sample_size:
    :param max_synapses_per_segment:
    :param permanence_increment:
    :param permanence_decrement:
    :param predicted_decrement:
    :param default_permanence:
    :param connected_permanence:
    :param learning_enabled:
    :param distal_activation_threshold:
    :param distal_matching_threshold:
    :param num_cells:
    :return:
    """

    """
    from:
    https://github.com/numenta/nupic/blob/master/src/nupic/frameworks/opf/common_models/anomaly_params_random_encoder/best_single_metric_anomaly_params_cpp.json
    "tmParams": {
        "activationThreshold": 13,
        "cellsPerColumn": 32,
        "columnCount": 2048,
        "globalDecay": 0.0,
        "initialPerm": 0.21,
        "inputWidth": 2048,
        "maxAge": 0,
        "maxSegmentsPerCell": 128,
        "maxSynapsesPerSegment": 32,
        "minThreshold": 10,
        "newSynapseCount": 20,
        "outputType": "normal",
        "pamLength": 3,
        "permanenceDec": 0.1,
        "permanenceInc": 0.1,
        "seed": 1960,
        "temporalImp": "cpp",
        "verbosity": 0
    },
    """
    TMDendriteSegment.new_synapse_sample_size = new_synapse_sample_size  # 41  # TODO: reconcile here
    TMDendriteSegment.max_synapses_per_segment = max_synapses_per_segment  # ?  2*sp.num_winning_columns?
    TMDendriteSegment.permanence_increment = permanence_increment
    TMDendriteSegment.permanence_decrement = permanence_decrement
    TMDendriteSegment.predicted_decrement = predicted_decrement
    TMDendriteSegment.default_permanence = default_permanence
    TMDendriteSegment.connected_permanence = connected_permanence

    # TMCell.max_segments_per_cell = 128  # TODO: implement
    TMCell.learning_enabled = learning_enabled
    TMCell.distal_activation_threshold = distal_activation_threshold
    TMCell.distal_matching_threshold = distal_matching_threshold

    TMColumn.num_cells = num_cells

    tm_column_addresses = range(num_columns)
    tm = TemporalMemory(tm_column_addresses)

    return tm


def process_file(file_path, out_path, encoder=None, sp=None, tm=None):
    logger.info("Processing {} to {}".format(file_path, out_path))

    # Input encoder setup
    if encoder is None:
        with open(file_path) as f:
            reader = csv.DictReader(f)
            values = [float(r["value"]) for r in reader]
        max_value = max(values)
        min_value = min(values)
        encoder = get_multi_encoder(min_value, max_value)

    # Spatial Pooler setup
    sp = get_spatial_pooler(encoder.num_bits) if sp is None else sp

    # Temporal Memory setup
    tm = get_nova_temporal_memory(len(sp.columns)) if tm is None else tm

    prediction_errors = []
    with open(file_path) as f:
        reader = csv.DictReader(f)

        for i, row in enumerate(reader):
            # print(i, row)
            value = float(row["value"])
            time_stamp = datetime.strptime(
                row["timestamp"], "%Y-%m-%d %H:%M:%S"
            )  # '2014-04-01 00:00:00'
            encoded_sdr = encoder.encode([value, time_stamp])
            sp_output_sdr = sp.process_input(encoded_sdr)

            tm_predicted = tm._col_predicted_sdr
            tm.process_input(sp_output_sdr)
            tm_actual = sp_output_sdr

            n = tm_predicted.overlap(tm_actual)
            d = len(tm_actual)
            prediction_error = (1 - (n / d)) if d > 0 else 1.0

            # Is this calculated / updated during the NAB optimize phase?
            # Pretty sure this isn't really needed.
            label = (
                1.0
                if prediction_error > 0.3
                else (0.5 if prediction_error > 0.1 else 0.0)
            )

            prediction_errors.append(
                {
                    "timestamp": row["timestamp"],
                    "anomaly_score": prediction_error,
                    "value": value,
                    "label": label,
                }
            )

            if i % 1000 == 0:
                print(
                    "{}\t{}\t{}\t{}|{}\t{}".format(
                        str(i).ljust(5),
                        "{:.02}".format(prediction_error).ljust(4),
                        str(len(tm_actual)).ljust(3),
                        row["timestamp"],
                        row["value"][:7].ljust(7),
                        "".join(
                            "-" if x == "0" else "|"
                            for x in encoded_sdr.as_binary_string(encoder.num_bits)
                        ),
                    )
                )
                dendrite_counts = []
                synapse_counts = []
                for col in tm.columns.values():
                    for cell in col.cells.values():
                        dendrite_counts.append(len(cell.distal_segments))
                        for seg in cell.distal_segments.values():
                            synapse_counts.append(len(seg._potential_permanences))

                print(
                    "\tDendrite:{}\t{}\t{}\t||\tSynapse:{}\t{}\t{}".format(
                        min(dendrite_counts),
                        round(sum(dendrite_counts) / float(len(dendrite_counts)), 3),
                        max(dendrite_counts),
                        min(synapse_counts),
                        round(sum(synapse_counts) / float(len(synapse_counts)), 3),
                        max(synapse_counts),
                    )
                )

            # if i == 100 and sum([1 for c in tm.columns if c.is_proximally_active]) < 10:
            #     print(i, sum([1 for c in tm.columns if c.is_proximally_active]))
            #     break

            # if i > 100:
            #     break

            # if i >= 3600:
            #     break

    with open(out_path, "w") as o:
        writer = csv.DictWriter(
            o, ["timestamp", "value", "anomaly_score", "label"], lineterminator="\n"
        )
        writer.writeheader()
        for row in prediction_errors:
            writer.writerow(row)

    return file_path, out_path, True


def main(key='nova'):
    logging.basicConfig(format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
    logger.setLevel(logging.DEBUG)

    # file_path = "data/artificialNoAnomaly/art_flatline.csv"
    # file_path = "data/artificialWithAnomaly/art_daily_jumpsdown.csv"
    # file_path = "data/artificialWithAnomaly/art_daily_flatmiddle.csv"
    # out_root = "./"
    # process_file(file_path, out_root)

    # file_limiters = [
    #     "art_daily_small_noise.csv",
    #     "art_increase_spike_density.csv",
    #     "exchange-3_cpc_results.csv",
    #     "ec2_cpu_utilization_77c1ca.csv",
    #     "speed_7578.csv",
    #     # "Twitter_volume_GOOG.csv",
    # ]

    in_paths = []
    base_root = "data/"
    for root, dirs, files in os.walk(base_root):
        for f in files:
            if f.endswith(".csv"):  # and any(f.endswith(x) for x in file_limiters):
                in_paths.append(root + "/" + f)

    from pprint import pprint

    pprint(in_paths)

    out_paths = [
        os.path.split(s)[0].replace("data", "results/{}".format(key))
        + "/{}_".format(key)
        + os.path.split(s)[1]
        for s in in_paths
    ]
    print("-" * 30)
    pprint(list(zip(in_paths, out_paths)))

    # for in_file, out_file in zip(in_paths, out_paths):
    #     out_root, _ = os.path.split(out_file)
    #     os.makedirs(out_root, exist_ok=True)
    #     if not os.path.isfile(out_file):
    #         process_file(in_file, out_file)
    #     # break

    from multiprocessing import Pool

    with Pool(processes=6) as pool:
        args = []
        for in_file, out_file in zip(in_paths, out_paths):
            out_root, _ = os.path.split(out_file)
            os.makedirs(out_root, exist_ok=True)
            if not os.path.isfile(out_file):
                args.append((in_file, out_file))

        result = pool.starmap(process_file, args)
        for i in result:
            print(i)


if __name__ == "__main__":
    main()
