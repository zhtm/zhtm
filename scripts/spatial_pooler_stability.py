from collections import Counter

from zhtm.spatial_pooler import NaiveRegion
from zhtm.sdr import SDR


def main() -> None:

    for percentage in (0.8, 0.7, 0.6, 0.5, 0.4, 0.3, 0.2):
        num_input_bits = 625  # 625
        num_columns = 2048
        num_potential_synapses_per_column = int(num_input_bits * percentage)

        column_config = {
            "permanence_increment": 0.1,
            "permanence_decrement": 0.1,
            "overlap_duty_cycle_low_increment": 0.001,
        }
        n = NaiveRegion(
            num_input_bits,
            num_columns,
            winning_column_count=41,
            column_config=column_config,
        )
        n.initialize_column_synapses(num_potential_synapses_per_column)
        n.boost_strength = 0.0  # Do not adjust active duty cycle
        n.stimulus_threshold = 0

        arbitrary_input = SDR.random(num_input_bits, 51)

        the_count = Counter()
        for _ in range(100):
            out_sdr = n.process_input(arbitrary_input)
            out_format = "|".join(["%02i" % x for x in sorted(list(out_sdr))])
            the_count.update([tuple(sorted(list(out_sdr)))])

        print("{}\t{}".format(percentage, len(the_count)))


if __name__ == "__main__":
    main()
