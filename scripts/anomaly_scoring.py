import csv
import json
import logging
import math
import os
from pprint import pformat

logger = logging.getLogger(__name__)


def load_windows(path="combined_windows.json"):
    with open(path) as f:
        return json.load(f)


def sigmoid(x):
    return 1 / (1 + math.exp(-x))


def scaled_sigmoid(p):
    if p > 3.0:
        return -1
    else:
        return 2 * sigmoid(-5.0 * p) - 1.0


def get_raw_scores(anomaly_file, cost_matrix):
    fn_weight = cost_matrix['fn_weight']
    tp_weight = cost_matrix['tp_weight']
    fp_weight = cost_matrix['fp_weight']

    logger.info("Scoring file {}".format(anomaly_file))
    all_windows = load_windows()

    window_name = [x for x in all_windows.keys() if anomaly_file.endswith(os.path.split(x)[1])][0]
    windows = all_windows[window_name]
    window_edges = [date_str for sublist in windows for date_str in sublist]

    # logger.info("Using window_name:{}".format(window_name))
    # logger.info(window_edges)

    # Load the CSV with scores and stuff
    with open(anomaly_file) as csvfile:
        reader = csv.DictReader(csvfile)
        rows = [x for x in reader]

    timestamps = [x['timestamp'] for x in rows]
    anomaly_scores = [x['anomaly_score'] for x in rows]

    window_widths = {"{}|{}".format(window_name, x[0]): float(
        timestamps.index(x[1].split('.')[0]) - timestamps.index(x[0].split('.')[0]) + 1) for x in windows}
    score_parts = {"{}|{}".format(window_name, x[0]): -fn_weight for x in windows}

    logger.info('num rows: {}\tnum windows: {}'.format(len(rows), len(window_widths)))
    # logger.info("{}".format(pformat(score_parts)))
    # logger.info("{}".format(pformat(window_widths)))

    # logger.info('-' * 50)
    # logger.info('Start row-by-row processing')
    cur_window = None
    cur_window_right_index = None
    cur_window_width = None
    prev_window_width = None
    preceding_window_right_index = None
    probation_percent = 0.15
    probationary_length = min(math.floor(probation_percent * len(rows)), probation_percent * 5000)
    max_tp = scaled_sigmoid(-1.0)

    value_bag = []
    for i, cur_timestamp in enumerate(timestamps):
        at_window_edge = False
        edge = None
        do_exit = False

        unscaled_score = None
        scaled_score = None

        # If we detect a window edge
        if len(window_edges) > 0 and cur_timestamp == window_edges[0].split('.')[0]:
            logger.debug("Window edge: {}\t{}\t{}".format(i, cur_timestamp, window_edges[0]))
            edge = window_edges.pop(0)

            at_window_edge = True

        if at_window_edge:
            # left edge of new window
            if cur_window is None:
                cur_window = "{}|{}".format(window_name, edge)
                cur_window_right_index = timestamps.index(window_edges[0].split('.')[0])
                cur_window_width = window_widths[cur_window]
                # assert cur_window_width == cur_window_right_index - i + 1  # +1 to include right edge
                logger.debug("Entered window: {}".format(cur_window))

            # right edge of current window
            elif cur_window is not None:
                do_exit = True

        # In a window: score as if true positive
        if cur_window is not None:
            # NOTE: the original method is:
            #   -(cur_window_right_index - i + 1) / cur_window_width
            # However, this results in positions (and thus scores) that shift beyond [-1, 0].
            # I removed the '+ 1' to get positions in [-1, 0]
            position_in_window = -(cur_window_right_index - i + 1) / cur_window_width
            unscaled_score = scaled_sigmoid(position_in_window)
            scaled_score = unscaled_score * tp_weight / max_tp
            # logger.debug("{}\t{}\t{}\t{}".format(i, round(position_in_window, 3), round(scaled_score, 3), round(unscaled_score, 3)))

        # Outside window: score as if false positive
        else:
            if preceding_window_right_index is None:
                position_in_window = 3.0
            else:
                position_in_window = abs(preceding_window_right_index - i) / float(prev_window_width - 1)

            unscaled_score = scaled_sigmoid(position_in_window)
            scaled_score = unscaled_score * fp_weight

        if i < probationary_length:
            scaled_score = None

        if i < 50:
            logger.debug("{}\t{}\t{}\t{}".format(i, round(position_in_window, 3), round(scaled_score, 3) if scaled_score is not None else scaled_score,
                                             round(unscaled_score, 3)))
        value_bag.append((i, cur_timestamp, scaled_score, unscaled_score, cur_window, anomaly_scores[i]))

        if do_exit:
            logger.debug("Exiting window: {}".format(cur_window))
            cur_window = None
            prev_window_width = cur_window_width
            cur_window_width = None
            preceding_window_right_index = i

    return value_bag, score_parts


def score_data(values, score_parts):
    values = sorted(values, key=lambda tup: tup[5], reverse=True)
    scores_by_threshold = []
    cur_threshold = 1.01  # Start with 'no values'
    prev_threshold = None

    tn = sum(1 if x[4] is None and x[2] is not None else 0 for x in values)
    fn = sum(1 if x[4] is not None else 0 for x in values)
    tp = 0
    fp = 0

    for i, tup in enumerate(values):
        _, timestamp, scaled_score, unscaled_score, cur_window, anomaly = tup

        if anomaly != cur_threshold:
            cur_score = sum(score_parts.values())
            logger.debug("New threshold: {}\tCurrent score: {}".format(anomaly, cur_score))
            if cur_threshold is not None:
                scores_by_threshold.append({
                    "threshold": cur_threshold,
                    "score": sum(score_parts.values()),
                    "tp": tp,
                    "tn": tn,
                    "fp": fp,
                    "fn": fn,
                    "total": tp + tn + fp + fn
                })
            cur_threshold = anomaly

        if scaled_score is not None:
            if cur_window is not None:
                tp += 1
                fn -= 1
            else:
                fp += 1
                tn -= 1

        # if i < 20:
        #     logger.debug("{}\t{}\t{}".format(i, (tp, tn, fp, fn), tup))

        if cur_window is None:
            score_parts["fp"] += scaled_score if scaled_score is not None else 0
        else:
            if scaled_score is not None:
                score_parts[cur_window] = max(score_parts[cur_window], scaled_score)
    else:
        scores_by_threshold.append({
            "threshold": cur_threshold,
            "score": sum(score_parts.values()),
            "tp": tp,
            "tn": tn,
            "fp": fp,
            "fn": fn,
            "total": tp + tn + fp + fn
        })

    t = sorted(scores_by_threshold, key=lambda d: d['score'], reverse=True)
    best = t[0]
    return best, scores_by_threshold


def main(key='numenta'):  # A92d2f051  flomp1
    # Standard score profile
    standard_cost_matrix = dict(
        fn_weight=1.0,
        tp_weight=1.0,
        fp_weight=0.11,
    )
    # key = 'oort6'

    # file_limiters = [
    #     "art_daily_small_noise.csv",
    #     "art_increase_spike_density.csv",
    #     "exchange-3_cpc_results.csv",
    #     "ec2_cpu_utilization_77c1ca.csv",
    #     "speed_7578.csv",
    #     #### "Twitter_volume_GOOG.csv",
    # ]

    file_limiters = [
        "art_load_balancer_spikes.csv",
        "iio_us-east-1_i-a2eb1cd9_NetworkIn.csv",
        "ec2_cpu_utilization_fe7f93.csv",
        "ec2_disk_write_bytes_c0d644.csv",
        "art_increase_spike_density.csv",
        "speed_7578.csv",
        "exchange-3_cpc_results.csv",
        "speed_t4013.csv",
        "ec2_cpu_utilization_5f5533.csv",
        "ec2_request_latency_system_failure.csv",
    ]

    base_root = 'results/{}'.format(key)
    file_paths = []
    for root, dirs, files in os.walk(base_root):
        for f in files:
            if f.endswith(".csv") and not root.endswith(key): # and any(f.endswith(x) for x in file_limiters):
                file_paths.append(root + "/" + f)
    logger.info("{}".format(pformat(file_paths)))
    logger.info("Num files detected: {}".format(len(file_paths)))
    #
    # file_to_score = "results/numenta/artificialWithAnomaly/numenta_art_increase_spike_density.csv"
    # file_to_score = "results/numenta/artificialWithAnomaly/numenta_art_daily_flatmiddle.csv"
    # file_to_score = "results/numenta/artificialWithAnomaly/numenta_art_daily_jumpsdown.csv"
    # file_to_score = "results/numenta/realAWSCloudwatch/numenta_rds_cpu_utilization_cc0c53.csv"
    # file_paths = [file_to_score]
    # -1.90387186017

    score_parts = {'fp': 0}
    all_values = []
    for file_to_score in file_paths:
        value_bag, sp = get_raw_scores(file_to_score, standard_cost_matrix)

        sp2 = {k: v for k, v in sp.items()}
        sp2['fp'] = 0
        best, sbt = score_data(value_bag, sp2)
        # logger.info("{},{},{},{},{},{},{},{},{},{}".format(
        #     key,
        #     'standard',
        #     file_to_score,
        #     best['threshold'],
        #     best['score'],
        #     best['tp'],
        #     best['tn'],
        #     best['fp'],
        #     best['fn'],
        #     best['total']
        # ))
        # for i in range(5):
        #     logger.info(sbt[i])
        logger.info("File: {}\tBest: {}".format(file_to_score, best))
        score_parts.update(sp)
        all_values.extend(value_bag)
        # break

    logger.info("-" * 50)
    logger.info("Time to score!")
    logger.debug("Num records: {}".format(len(all_values)))

    all_values = sorted(all_values, key=lambda tup: tup[5], reverse=True)

    scores_by_threshold = []
    cur_threshold = 1.01  # Start with 'no values'
    prev_threshold = None

    tn = sum(1 if x[4] is None and x[2] is not None else 0 for x in all_values)
    fn = sum(1 if x[4] is not None else 0 for x in all_values)
    tp = 0
    fp = 0

    for i, tup in enumerate(all_values):
        _, timestamp, scaled_score, unscaled_score, cur_window, anomaly = tup

        if anomaly != cur_threshold:
            cur_score = sum(score_parts.values())
            logger.debug("New threshold: {}\tCurrent score: {}".format(anomaly, cur_score))
            if cur_threshold is not None:
                scores_by_threshold.append({
                    "threshold": cur_threshold,
                    "score": sum(score_parts.values()),
                    "tp": tp,
                    "tn": tn,
                    "fp": fp,
                    "fn": fn,
                    "total": tp + tn + fp + fn
                })
            cur_threshold = anomaly

        if scaled_score is not None:
            if cur_window is not None:
                tp += 1
                fn -= 1
            else:
                fp += 1
                tn -= 1

        if i < 20:
            logger.debug("{}\t{}\t{}".format(i, (tp, tn, fp, fn), tup))

        if cur_window is None:
            score_parts["fp"] += scaled_score if scaled_score is not None else 0
        else:
            if scaled_score is not None:
                score_parts[cur_window] = max(score_parts[cur_window], scaled_score)
    else:
        scores_by_threshold.append({
            "threshold": cur_threshold,
            "score": sum(score_parts.values()),
            "tp": tp,
            "tn": tn,
            "fp": fp,
            "fn": fn,
            "total": tp + tn + fp + fn
        })

    logger.info("-" * 50)
    for i, v in enumerate(scores_by_threshold):
        logger.info("{}\t{}".format(i, v))
        if i > 10:
            break

    with open('score_by_threshold_{}.csv'.format(key), 'w') as o:
        writer = csv.DictWriter(o, fieldnames=['threshold', 'score', 'tp', 'tn', 'fp', 'fn', 'total'])
        writer.writeheader()
        writer.writerows(scores_by_threshold)

    t = sorted(scores_by_threshold, key=lambda d: d['score'], reverse=True)
    logger.info("Best: {}".format(t[0]))
    return t[0]['score'], t[0]['threshold']


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
    logger.setLevel(logging.INFO)
    from datetime import datetime
    s = datetime.now()
    main()
    e = datetime.now()
    logger.warning("Total time: {}".format(e - s))
