import csv
from datetime import datetime
import logging
import os

import numpy

from zhtm.encoders.scalar_encoder import ScalarEncoder
from zhtm.encoders.datetime_encoders import HourOfDayEncoder
from zhtm.encoders.multi_encoder import MultiEncoder
from zhtm.spatial_pooler import NaiveSpatialPooler

logger = logging.getLogger(__name__)


class EncoderStats:
    def __init__(self, min_buckets, max_buckets, num_active_bits, hour_width, hour_radius):
        self.min_buckets = min_buckets
        self.max_buckets = max_buckets
        self.num_active_bits = num_active_bits
        self.scalar = None

        self.hour_width = hour_width
        self.hour_radius = hour_radius
        self.hour = HourOfDayEncoder(hour_width, hour_radius)

        self.multi = None

    def _get_scalar_encoder(self, min_val, max_val,):
        range_padding = abs(max_val - min_val) * 0.2
        min_val, max_val = min_val - range_padding, max_val + range_padding
        if min_val == max_val:
            min_val -= 50
            max_val += 50
        num_buckets = min(self.max_buckets, max(self.min_buckets, int(1 * (max_val - min_val))))

        # print("Encoder: ({},{}), buckets: {}".format(min_val, max_val, num_buckets))
        s = ScalarEncoder(
            min_val=min_val,
            max_val=max_val,
            num_active_bits=self.num_active_bits,
            num_buckets=num_buckets,
        )
        return s

    def process_sdrs(self, min_value, max_value, values, timestamps):
        min_value, max_value = min(min_value, max_value), max(min_value, max_value)
        self.scalar = self._get_scalar_encoder(min_value, max_value)
        self.multi = MultiEncoder([self.scalar, self.hour])

        encoder = self.scalar
        # encoder = self.multi

        # from zhtm.dendrite_segment import DendriteSegment
        # DendriteSegment.permanence_increment = 0.003
        # DendriteSegment.permanence_decrement = 0.0005
        # num_input_bits = encoder.num_bits
        # num_columns = 2048
        # stimulus_threshold = 0
        # winning_column_count = 0
        # min_overlap_duty_cycle_percent = 0.001
        # boost_strength = 0
        # percent_potential = 0.2
        # sp = NaiveSpatialPooler(
        #     num_input_bits,
        #     num_columns,
        #     stimulus_threshold,
        #     winning_column_count,
        #     min_overlap_duty_cycle_percent,
        #     boost_strength,
        # )
        # sp.randomize_column_synapses(int(num_input_bits * percent_potential))

        sdrs = []
        for cur_val, cur_timestamp in zip(values, timestamps):
            # cur_sdr = encoder.encode([cur_val, cur_timestamp])
            cur_sdr = encoder.encode(cur_val)
            # cur_sdr = sp.process_input(cur_sdr)
            sdrs.append(cur_sdr)
        return sdrs

    def process_file(self, file_name):
        with open(file_name) as f:
            reader = csv.DictReader(f)
            records = [r for r in reader]
            timestamps = [datetime.strptime(r["timestamp"], "%Y-%m-%d %H:%M:%S") for r in records]
            values = [float(r['value']) for r in records]

        max_value = max(values)
        min_value = min(values)

        sdrs = self.process_sdrs(min_value, max_value, values, timestamps)
        overlaps = [sdrs[i].overlap(sdrs[i-1]) if i != 0 else None for i in range(len(sdrs))]
        diffs = [abs(values[i]-values[i-1]) if i != 0 else None for i in range(len(values))]

        cor = numpy.corrcoef(overlaps[1:], diffs[1:])[0, 1]
        return cor
        # logger.critical(len(sdrs[1]))
        # import numpy
        # logger.info(overlaps[:10])
        # logger.info(diffs[:10])
        # logger.info(numpy.corrcoef(overlaps[1:], diffs[1:])[0, 1])
        #
        # with open('variances.csv', 'w') as o:
        #     writer = csv.DictWriter(o, ['overlap', 'diff'])
        #     writer.writeheader()
        #     writer.writerows([{'overlap': ov, 'diff': d} for ov, d in zip(overlaps[1:], diffs[1:])])

    def process_corpus(self, root='data'):
        file_excludes = [
            "art_flatline.csv",
        ]

        in_paths = []
        base_root = "data/"
        for root, dirs, files in os.walk(base_root):
            for f in files:
                if f.endswith(".csv") and not any(f.endswith(x) for x in file_excludes):
                    in_paths.append(root + "/" + f)

        correlations = []
        for i, path in enumerate(in_paths):
            correlations.append(self.process_file(path))

        t = zip(correlations, in_paths)
        t = sorted(t, reverse=True)
        vals = [x[0] for x in t]
        from pprint import pprint
        pprint(t)
        print(max(vals) - min(vals))
        print(numpy.average(vals))
        return max(vals)


def main():
    min_buckets = 450
    max_buckets = 450
    num_active_bits = 461
    hour_width = 4
    hour_radius = 32  # 32
    t = EncoderStats(min_buckets, max_buckets, num_active_bits, hour_width, hour_radius)

    # f = 'data/artificialWithAnomaly/art_daily_jumpsup.csv'
    # t.process_file(f)
    logger.info(t.process_corpus())


def score_func(min_buckets, max_buckets, num_active_bits):
    t = EncoderStats(min_buckets, max_buckets, num_active_bits, 1, 1)
    score = t.process_corpus()
    print('[{},{}], {} ==> {}'.format(min_buckets, max_buckets, num_active_bits, score))
    return score  # minimize 'max val'


def optimize():
    from nevergrad import instrumentation
    from nevergrad.optimization import optimizerlib
    min_buckets = instrumentation.variables.OrderedDiscrete(list(range(50, 601, 200)))
    max_buckets = instrumentation.variables.OrderedDiscrete(list(range(50, 1201, 200)))
    num_active_bits = instrumentation.variables.OrderedDiscrete(list(range(10, 601, 41)))

    some_func = instrumentation.InstrumentedFunction(
        score_func,
        min_buckets,
        max_buckets,
        num_active_bits
    )

    optimizer = optimizerlib.OnePlusOne(3, 20, num_workers=1)
    recommendation = optimizer.optimize(some_func, executor=None, batch_mode=False)
    args, kwargs = some_func.convert_to_arguments(recommendation)
    print(args)
    print(kwargs)


if __name__ == "__main__":
    logging.basicConfig(format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
    logger.setLevel(logging.DEBUG)
    logger.info("Starting!")
    main()
    # optimize()
