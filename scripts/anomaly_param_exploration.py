import csv
from datetime import datetime
from functools import partial
import json
import logging
from multiprocessing import Pool
import os
from uuid import uuid4

from nevergrad import instrumentation as instru
from nevergrad.optimization import optimizerlib
from zhtm.encoders.datetime_encoders import HourOfDayEncoder
from zhtm.encoders.multi_encoder import MultiEncoder

from anomaly_exploration import (
    process_file,
    get_encoder,
    get_spatial_pooler,
    get_nova_temporal_memory
)
from anomaly_scoring import main as score
logger = logging.getLogger(__name__)


def process_single_file(
        in_file,
        out_file,
        min_buckets=150,
        max_buckets=300,
        num_active_bits=160,
        hour_width=6,
        hour_radius=3,
        num_columns=2048,
        winning_column_count=41,
        percent_potential=0.8,
        new_synapse_sample_size=41,
        max_synapses_per_segment=1400,
        permanence_increment=0.1,
        permanence_decrement=0.03,
        predicted_decrement=0.001,
        distal_activation_threshold=20,
        distal_matching_threshold=10,
        num_cells=4
):
    min_buckets, max_buckets = min(min_buckets, max_buckets), max(min_buckets, max_buckets)

    dat = distal_activation_threshold
    dmt = distal_matching_threshold
    dat, dmt = max(dat, dmt), min(dat, dmt)
    distal_activation_threshold = dat
    distal_matching_threshold = dmt

    if max_synapses_per_segment < distal_activation_threshold:
        return in_file, out_file, False

    # Get min / max values for encoder
    with open(in_file) as f:
        reader = csv.DictReader(f)
        values = [float(r["value"]) for r in reader]
    max_value = max(values)
    min_value = min(values)
    scalar_encoder = get_encoder(min_value, max_value, min_buckets, max_buckets, num_active_bits)
    # hour_encoder = HourOfDayEncoder(hour_width, hour_radius)
    # encoder = MultiEncoder([scalar_encoder, hour_encoder])
    encoder = scalar_encoder

    # Spatial Pooler setup
    sp = get_spatial_pooler(
        encoder.num_bits,
        num_columns=num_columns,
        winning_column_count=winning_column_count,
        percent_potential=percent_potential,
    )

    # Temporal Memory setup
    tm = get_nova_temporal_memory(
        num_columns,
        new_synapse_sample_size=new_synapse_sample_size,
        max_synapses_per_segment=max_synapses_per_segment,
        permanence_increment=permanence_increment,
        permanence_decrement=permanence_decrement,
        predicted_decrement=predicted_decrement,
        distal_activation_threshold=distal_activation_threshold,
        distal_matching_threshold=distal_matching_threshold,
        num_cells=num_cells
    )

    prediction_errors = []
    with open(in_file) as f:
        reader = csv.DictReader(f)

        for i, row in enumerate(reader):
            # print(i, row)
            value = float(row["value"])
            time_stamp = datetime.strptime(
                row["timestamp"], "%Y-%m-%d %H:%M:%S"
            )  # '2014-04-01 00:00:00'
            # encoded_sdr = encoder.encode([value, time_stamp])
            encoded_sdr = encoder.encode(value)
            sp_output_sdr = sp.process_input(encoded_sdr)

            tm_predicted = tm._col_predicted_sdr
            tm.process_input(sp_output_sdr)
            tm_actual = sp_output_sdr

            n = tm_predicted.overlap(tm_actual)
            d = len(tm_actual)
            prediction_error = (1 - (n / d)) if d > 0 else 1.0

            # Is this calculated / updated during the NAB optimize phase?
            # Pretty sure this isn't really needed.
            label = (
                1.0
                if prediction_error > 0.3
                else (0.5 if prediction_error > 0.1 else 0.0)
            )

            prediction_errors.append(
                {
                    "timestamp": row["timestamp"],
                    "anomaly_score": prediction_error,
                    "value": value,
                    "label": label,
                }
            )

            if i % 1000 == 0:
                print(
                    "{}\t{}\t{}\t{}|{}\t{}".format(
                        str(i).ljust(5),
                        "{:.02}".format(prediction_error).ljust(4),
                        str(len(tm_actual)).ljust(3),
                        row["timestamp"],
                        row["value"][:7].ljust(7),
                        "".join(
                            "-" if x == "0" else "|"
                            for x in encoded_sdr.as_binary_string(encoder.num_bits)
                        ),
                    )
                )
                dendrite_counts = []
                synapse_counts = []
                for col in tm.columns.values():
                    for cell in col.cells.values():
                        dendrite_counts.append(len(cell.distal_segments))
                        for seg in cell.distal_segments.values():
                            synapse_counts.append(len(seg._potential_permanences))

                print(
                    "\tDendrite:{}\t{}\t{}\t||\tSynapse:{}\t{}\t{}".format(
                        min(dendrite_counts),
                        round(sum(dendrite_counts) / float(len(dendrite_counts)), 3),
                        max(dendrite_counts),
                        min(synapse_counts),
                        round(sum(synapse_counts) / float(len(synapse_counts)), 3),
                        max(synapse_counts),
                    )
                )

    with open(out_file, "w") as o:
        writer = csv.DictWriter(
            o, ["timestamp", "value", "anomaly_score", "label"], lineterminator="\n"
        )
        writer.writeheader()
        for row in prediction_errors:
            writer.writerow(row)

    logger.info("Done with {}".format(out_file))
    return in_file, out_file, True


def run_and_score(
        min_buckets=450,
        max_buckets=450,
        num_active_bits=461,
        hour_width=12,
        hour_radius=9,
        num_columns=2048,
        winning_column_count=161,
        percent_potential=0.2,
        new_synapse_sample_size=50,
        max_synapses_per_segment=2048,
        permanence_increment=0.05,
        permanence_decrement=0.03,
        predicted_decrement=0.00,
        distal_activation_threshold=10,
        distal_matching_threshold=5,
        num_cells=8,
        num_processes=5
):
    key = 'A'+str(uuid4())[:8]

    # file_limiters = [
    #     "art_daily_small_noise.csv",
    #     "art_increase_spike_density.csv",
    #     "exchange-3_cpc_results.csv",
    #     "ec2_cpu_utilization_77c1ca.csv",
    #     "speed_7578.csv",
    #     # "Twitter_volume_GOOG.csv",
    # ]

    file_limiters = [
        "art_load_balancer_spikes.csv",
        "iio_us-east-1_i-a2eb1cd9_NetworkIn.csv",
        "ec2_cpu_utilization_fe7f93.csv",
        "ec2_disk_write_bytes_c0d644.csv",
        "art_increase_spike_density.csv",
        "speed_7578.csv",
        "exchange-3_cpc_results.csv",
        "speed_t4013.csv",
        "ec2_cpu_utilization_5f5533.csv",
        "ec2_request_latency_system_failure.csv",
    ]

    in_paths = []
    base_root = "data/"
    for root, dirs, files in os.walk(base_root):
        for f in files:
            if f.endswith(".csv") and any(f.endswith(x) for x in file_limiters):
                in_paths.append(root + "/" + f)

    out_paths = [
        os.path.split(s)[0].replace("data", "results/{}".format(key))
        + "/{}_".format(key)
        + os.path.split(s)[1]
        for s in in_paths
    ]
    config_dict = dict(
        min_buckets=min_buckets,
        max_buckets=max_buckets,
        num_active_bits=num_active_bits,
        hour_width=hour_width,
        hour_radius=hour_radius,
        num_columns=num_columns,
        winning_column_count=winning_column_count,
        percent_potential=percent_potential,
        new_synapse_sample_size=new_synapse_sample_size,
        max_synapses_per_segment=max_synapses_per_segment,
        permanence_increment=permanence_increment,
        permanence_decrement=permanence_decrement,
        predicted_decrement=predicted_decrement,
        distal_activation_threshold=distal_activation_threshold,
        distal_matching_threshold=distal_matching_threshold,
        num_cells=num_cells,
    )
    partial_process = partial(process_single_file, **config_dict)

    with Pool(processes=num_processes) as pool:
        args = []

        for in_file, out_file in zip(in_paths, out_paths):
            out_root, _ = os.path.split(out_file)
            os.makedirs(out_root, exist_ok=True)
            if not os.path.isfile(out_file):
                args.append((in_file, out_file))

        result = pool.starmap(partial_process, args)
        for i in result:
            print(i)

    if any(x[2] is False for x in result):
        best_score = -1000
        best_threshold = None
    else:
        best_score, best_threshold = score(key)

    with open('results/while_optimizing.csv', 'a') as f:
        f.write('{}\t{}\t{}\t{}\n'.format(key, best_score, best_threshold, json.dumps(config_dict)))

    return -best_score  # Negative because optimizer is a minimizer


def main():
    min_buckets = 450
    max_buckets = 450
    num_active_bits = 502
    hour_width = 1  # disabled
    hour_radius = 1  # disabled
    num_columns = instru.variables.OrderedDiscrete([1024, 2048, 4096])
    winning_column_count = instru.variables.OrderedDiscrete([17, 65, 257])   # [17, 33, 65, 129, 257])
    percent_potential = instru.variables.OrderedDiscrete([.05, .2, .8])
    new_synapse_sample_size = instru.variables.OrderedDiscrete([16, 64, 256])
    max_synapses_per_segment = instru.variables.OrderedDiscrete([32, 256, 2048])
    permanence_increment = instru.variables.OrderedDiscrete([0.01, 0.03, 0.05])
    permanence_decrement = instru.variables.OrderedDiscrete([0.005, 0.01, 0.03])
    predicted_decrement = instru.variables.OrderedDiscrete([0.0, 0.0001, 0.001])
    distal_activation_threshold = instru.variables.OrderedDiscrete([8, 32, 128])
    distal_matching_threshold = instru.variables.OrderedDiscrete([4, 8, 32, 64, 128])
    num_cells = instru.variables.OrderedDiscrete([4, 16, 32, 64])
    num_processes = 6

    some_func = instru.InstrumentedFunction(
        run_and_score,
        min_buckets=min_buckets,
        max_buckets=max_buckets,
        num_active_bits=num_active_bits,
        hour_width=hour_width,
        hour_radius=hour_radius,
        num_columns=num_columns,
        winning_column_count=winning_column_count,
        percent_potential=percent_potential,
        new_synapse_sample_size=new_synapse_sample_size,
        max_synapses_per_segment=max_synapses_per_segment,
        permanence_increment=permanence_increment,
        permanence_decrement=permanence_decrement,
        predicted_decrement=predicted_decrement,
        distal_activation_threshold=distal_activation_threshold,
        distal_matching_threshold=distal_matching_threshold,
        num_cells=num_cells,
        num_processes=num_processes
    )

    optimizer = optimizerlib.OnePlusOne(11, 50, num_workers=1)
    recommendation = optimizer.optimize(some_func, executor=None, batch_mode=False)
    args, kwargs = some_func.convert_to_arguments(recommendation)
    print(args)
    print(kwargs)
    """
    i_func = instru.InstrumentedFunction(my_func, min_buckets, max_buckets, num_active_bits)

    optimizer = optimizerlib.OnePlusOne(3, 10, num_workers=1)
    recommendation = optimizer.optimize(i_func, executor=None, batch_mode=False)
    args, kwargs = i_func.convert_to_arguments(recommendation)
    print(args)
    print(kwargs)
    """


if __name__ == '__main__':
    logging.basicConfig(format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
    logger.setLevel(logging.INFO)
    logger.info("Go")
    main()
    # run_and_score()
