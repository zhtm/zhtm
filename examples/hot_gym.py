"""
Hot Gym example

This example requires the `bokeh` package to run. If you cloned `zhtm` locally, you can install all packages required
by the examples via:

```
pip install -e .[examples]
```

To run this example, you need to:
    1) get the 'hot gym' CSV file and
    2) run the `bokeh` server on this file.

## Getting the Hot Gym file.

This example file is a rough imitation of Numenta's own 'Hot Gym' example. Save this file to your local directory:
https://github.com/numenta/nupic/blob/master/examples/opf/clients/hotgym/prediction/one_gym/rec-center-hourly.csv

This script will, by default, look for the file as `./rec-center-hourly.csv`, but if you save it somewhere else, just
pass in the correct location during the call to `load_data()`.

## Running the bokeh server
If your python environment is set up correctly, you should be able to run the bokeh server by calling:
`bokeh serve --show examples/hot_gym.py`.  If you are on Windows and using a virtual environment, you will instead need
something like `venv\Scripts\bokeh-script.py serve --show examples/hot_gym.py`.  You can read more about bokeh servers
here: https://bokeh.pyddata.org/en/latest/docs/user_guide/server.html

If things work as expected, you should see your internet browser pop up with some graphs and other charts. Note that it
will probably take a few seconds to load initially as the CSV file is read.

You should see:
* A line graph at the top that displays the energy usage of the gym over time.
* A red dot which represents the 'selected input'.
* A matrix at the bottom left showing the input SDR for the currently selected time
* A matrix at the bottom right showing the output SDR from the spatial pooler for the currently selected time
* A 'selected index' slider in the bottom center, which starts at zero.
    * Note that the first 100 inputs are pre-computed...feel free to move the slider around.
* A 'process next input' button, which processes the next input (and the selects is, moving the red dot forward).
* Below the energy usage chart is an 'output similarity' bar chart, which, as the red dot moves forward, shows the 20
    previous inputs with output SDRs most similar to the current time's SDR.
* Along with the output-similarity bar chart, orange dots will appear on the main energy graph to show the 20 points in
    time that are considered 'most similar' to the current point (in red).
"""
from csv import DictReader
from datetime import datetime
import heapq
import logging
import math

from bokeh.layouts import layout, Row, Column
from bokeh.models import Button, Slider, ColumnDataSource
from bokeh.models.widgets import Paragraph
from bokeh.plotting import figure, curdoc

from zhtm.encoders.scalar_encoder import ScalarEncoder
from zhtm.encoders.datetime_encoders import (
    HourOfDayEncoder,
    DayOfWeekEncoder,
    WeekendEncoder,
)
from zhtm.encoders.multi_encoder import MultiEncoder
from zhtm.spatial_pooler import NaiveSpatialPooler


logger = logging.getLogger(__name__)


def load_data(file_path="rec-center-hourly.csv"):
    logger.debug("Loading data from {}".format(file_path))
    timestamps = []
    energy_readings = []
    with open(file_path) as f:
        reader = DictReader(f)
        next(reader)  # remove data type row
        next(reader)  # remove the 'T' row

        for row in reader:
            timestamps.append(datetime.strptime(row["timestamp"], "%m/%d/%y %H:%M"))
            energy_readings.append(float(row["kw_energy_consumption"]))

    return timestamps, energy_readings


def get_hot_gym_encoder():
    value_encoder = ScalarEncoder(
        min_val=0, max_val=100, num_active_bits=31, num_buckets=200
    )
    hour_of_day = HourOfDayEncoder(8, 16)
    day_of_week = DayOfWeekEncoder(8, 4)
    weekend = WeekendEncoder(24, 0)

    gym_encoder = MultiEncoder([value_encoder, hour_of_day, day_of_week, weekend])
    return gym_encoder


def get_spatial_pooler(num_bits_in_encoder):
    num_columns = 2048
    sp = NaiveSpatialPooler(
        num_bits_in_encoder,
        num_columns,
        stimulus_threshold=0,
        winning_column_count=41,
        min_overlap_duty_cycle_percent=0.001,
        boost_strength=0.0,
    )
    num_potential_synapses = int(0.8 * sp.num_input_bits)
    sp.randomize_column_synapses(num_potential_synapses)
    return sp


def sdr_to_count_dict(sdr, total_bits, n=8):
    rows = []
    columns = []
    counts = []

    counts_by_slots = [
        sum([1 for j in range(n * i, n * i + n) if j in sdr])
        for i in range(int(total_bits / n) + (1 if total_bits % n != 0 else 0))
    ]
    width = round(
        math.sqrt(len(counts_by_slots))
    )  # math.ceil(math.sqrt(len(counts_by_slots)))  # 32
    max_count = 0
    for i, count in enumerate(counts_by_slots):
        r = int(i / width)
        c = i % width
        max_count = max(max_count, count)

        rows.append(r)
        columns.append(c)
        counts.append(count)

    alphas = [0.1 + (0.9 * count / max_count) for count in counts]

    data = dict(
        row_name=[str(t) for t in columns],
        col_name=[str(t) for t in rows],
        # colors=color,
        alphas=alphas,
        counts=counts,
    )
    return data, width


class HotGym:
    def __init__(self):
        self.encoder = get_hot_gym_encoder()
        self.sp = get_spatial_pooler(self.encoder.num_bits)

        self.input_sdr_shrink_value = 1
        self.input_sdr_width = round(
            math.sqrt(
                int(self.encoder.num_bits / self.input_sdr_shrink_value)
                + (1 if self.encoder.num_bits % self.input_sdr_shrink_value != 0 else 0)
            )
        )
        self.input_sdrs = []
        self.input_sdr_dicts = []
        self.input_similarities = []
        self.output_similarities = []

        self.output_sdr_shrink_value = 1
        self.output_sdr_width = round(
            math.sqrt(
                int(len(self.sp.columns) / self.output_sdr_shrink_value)
                + (1 if len(self.sp.columns) % self.output_sdr_shrink_value != 0 else 0)
            )
        )
        self.output_sdrs = []
        self.output_sdr_dicts = []

        self.timestamps, self.energy_values = load_data()
        self.data_index = -1  # how far the pooler has processed
        self.input_sdr_width = None

        for _ in range(160):
            self.process_next_data_point()

        self._text_template = "Selected data:{}\n\nEnergy usage: {}"

        self.selected_index = 0
        self.selected_timestamp = None
        self.selected_energy = None
        self.selected_index_text = "TEXT NOT SET"
        self.selected_input_sdr_dict = None
        self.selected_output_sdr_dict = None
        self.selected_input_similarities = None
        self.selected_output_similarities = None

        self.set_selected_index(self.data_index)

    def update_selected_index_text(self):
        ts = self.timestamps[self.selected_index]
        energy = self.energy_values[self.selected_index]

        self.selected_index_text = self._text_template.format(
            self.selected_timestamp, self.selected_energy
        )

    def process_next_data_point(self):
        self.data_index += 1

        energy_value = self.energy_values[self.data_index]
        timestamp = self.timestamps[self.data_index]
        input_sdr = self.encoder.encode([energy_value, timestamp, timestamp, timestamp])
        output_sdr = self.sp.process_input(input_sdr)

        # Calculate similarities
        similar_inputs = heapq.nlargest(
            20,
            [
                (sdr.overlap(input_sdr), t, e)
                for sdr, t, e in zip(
                    self.input_sdrs, self.timestamps, self.energy_values
                )
            ],
        )
        similar_input_dict = dict(
            overlap=[x[0] for x in similar_inputs],
            timestamp=[x[1] for x in similar_inputs],
            energy=[x[2] for x in similar_inputs],
        )

        similar_outputs = heapq.nlargest(
            20,
            [
                (sdr.overlap(output_sdr), t, e)
                for sdr, t, e in zip(
                    self.output_sdrs, self.timestamps, self.energy_values
                )
            ],
        )
        similar_output_dict = dict(
            overlap=[x[0] for x in similar_outputs],
            timestamp=[x[1] for x in similar_outputs],
            energy=[x[2] for x in similar_outputs],
        )

        # Store input SDR
        input_sdr_dict, in_width = sdr_to_count_dict(
            input_sdr, self.encoder.num_bits, self.input_sdr_shrink_value
        )
        self.input_sdrs.append(input_sdr)
        self.input_sdr_dicts.append(input_sdr_dict)
        self.input_similarities.append(similar_input_dict)
        self.input_sdr_width = in_width

        # Store output SDR
        output_sdr_dict, out_width = sdr_to_count_dict(
            output_sdr, len(self.sp.columns), self.output_sdr_shrink_value
        )
        self.output_sdrs.append(output_sdr)
        self.output_sdr_dicts.append(output_sdr_dict)
        self.output_similarities.append(similar_output_dict)
        self.output_sdr_width = out_width

    def set_selected_index(self, new_index):
        logger.debug(
            "Set selected index: {} --> {}".format(self.selected_index, new_index)
        )

        self.selected_index = new_index
        self.selected_timestamp = self.timestamps[self.selected_index]
        self.selected_energy = self.energy_values[self.selected_index]
        self.update_selected_index_text()

        self.selected_input_sdr_dict = self.input_sdr_dicts[self.selected_index]
        self.selected_input_similarities = self.input_similarities[self.selected_index]

        self.selected_output_sdr_dict = self.output_sdr_dicts[self.selected_index]
        self.selected_output_similarities = self.output_similarities[
            self.selected_index
        ]


def main():
    gym = HotGym()

    # Configure data sources for several widgets
    selected_ds = ColumnDataSource(
        data={"energy": [gym.selected_energy], "timestamp": [gym.selected_timestamp]}
    )
    input_sdr_ds = ColumnDataSource(
        data={"row_name": ["0"], "col_name": ["0"], "alphas": [0.9], "counts": [1]}
    )
    output_sdr_ds = ColumnDataSource(
        data={"row_name": ["0"], "col_name": ["0"], "alphas": [0.9], "counts": [1]}
    )

    similar_input_ds = ColumnDataSource(data={"timestamp": [], "energy": []})

    similar_output_ds = ColumnDataSource(data={"timestamp": [], "energy": []})

    # ============================================================
    # Primary energy vs. time setup
    # ============================================================
    energy_fig = figure(
        title="Energy usage",
        x_axis_type="datetime",
        width=800,
        height=200,
        tools="pan,reset",
    )
    energy_fig.toolbar.logo = None
    energy_fig.step(gym.timestamps[:480], gym.energy_values[:480])
    energy_fig.scatter(x="timestamp", y="energy", source=selected_ds, color="red")
    # energy_fig.scatter(x='timestamp', y='energy', source=similar_input_ds, color='dimgray', alpha=0.5)
    energy_fig.scatter(
        x="timestamp",
        y="energy",
        source=similar_output_ds,
        color="goldenrod",
        alpha=0.9,
    )

    selected_index_slider = Slider(
        start=0, end=gym.data_index, value=0, step=1, title="Selected index"
    )
    selected_index_paragraph = Paragraph(text=gym.selected_index_text)

    process_button = Button(label="Process next point")

    overlap_fig = figure(
        title="Output Similarity",
        x_range=energy_fig.x_range,
        x_axis_type="datetime",
        y_range=[0, 30],
        width=800,
        height=60,
    )
    overlap_fig.grid.grid_line_color = None
    overlap_fig.axis.minor_tick_line_color = None
    overlap_fig.axis.major_label_text_font_size = "0pt"
    overlap_fig.vbar(x="timestamp", top="overlap", width=10, source=similar_output_ds)
    overlap_fig.toolbar.logo = None
    # ============================================================
    # Input SDR setup
    # ============================================================
    input_sdr_fig = figure(
        title="Input SDR",
        x_axis_location="above",
        tools="hover",
        x_range=[str(i) for i in range(gym.input_sdr_width)],
        y_range=[str(i) for i in range(gym.input_sdr_width, -1, -1)],
        tooltips=[("coordinate", "@row_name, @col_name"), ("count", "@counts")],
        toolbar_location=None,
        width=250,
        height=250,
    )
    input_sdr_fig.axis.major_label_text_font_size = "0pt"
    input_sdr_fig.axis.major_tick_line_color = None
    input_sdr_fig.grid.grid_line_color = None

    input_sdr_fig.rect(
        "row_name", "col_name", 0.9, 0.9, source=input_sdr_ds, alpha="alphas"
    )

    # ============================================================
    # Output SDR setup
    # ============================================================
    output_sdr_fig = figure(
        title="Output SDR",
        x_axis_location="above",
        x_range=[str(i) for i in range(gym.output_sdr_width)],
        y_range=[str(i) for i in range(gym.output_sdr_width, -1, -1)],
        tooltips=[("coordinate", "@row_name, @col_name"), ("count", "@counts")],
        toolbar_location=None,
        width=250,
        height=250,
    )
    output_sdr_fig.axis.major_label_text_font_size = "0pt"
    output_sdr_fig.axis.major_tick_line_color = None
    output_sdr_fig.grid.grid_line_color = None

    output_sdr_fig.rect(
        "row_name", "col_name", 0.9, 0.9, source=output_sdr_ds, alpha="alphas"
    )

    # ============================================================
    # Configure interactivity
    # ============================================================
    def selected_index_callback(attr, old, new):
        gym.set_selected_index(new)

        selected_index_paragraph.text = gym.selected_index_text
        selected_ds.data = {
            "energy": [gym.selected_energy],
            "timestamp": [gym.selected_timestamp],
        }

        input_sdr_ds.data = gym.selected_input_sdr_dict
        output_sdr_ds.data = gym.selected_output_sdr_dict

        similar_input_ds.data = gym.selected_input_similarities
        similar_output_ds.data = gym.selected_output_similarities

    selected_index_slider.on_change("value", selected_index_callback)

    selected_index_callback("value", 0, 0)

    def process_button_callback():
        gym.process_next_data_point()
        selected_index_slider.end = gym.data_index
        selected_index_slider.value = gym.data_index
        # selected_index_callback('value', gym.selected_index, gym.data_index)

    process_button.on_click(process_button_callback)
    # ============================================================
    # Final layout
    # ============================================================
    l = layout(
        Row(energy_fig),
        Row(overlap_fig),
        Row(
            input_sdr_fig,
            Column(
                Row(selected_index_slider),
                Row(selected_index_paragraph),
                Row(process_button),
            ),
            output_sdr_fig,
        ),
        sizing_mode="scale_width",
    )
    curdoc().add_root(l)


if __name__ != "__main__":
    logging.basicConfig(format="%(asctime)s\t%(name)s\t%(levelname)s\t%(message)s")
    logger.setLevel(logging.DEBUG)
    main()
