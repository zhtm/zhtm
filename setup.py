from distutils.core import setup

setup(
    name="zhtm",
    version="0.5dev",
    author="Zachary Cross",
    author_email="zachary.t.cross@gmail.com",
    description="Python implementation of Hierarchical Temporal Memory",
    long_description=open("README.md").read(),
    url="https://bitbucket.org/zhtm/zhtm/src",
    packages=["zhtm", "zhtm.encoders"],
    license="GNU Affero General Public License v3 or later (AGPLv3+)",
    classifiers=[
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU Affero General Public License v3 or later (AGPLv3+)",
    ],
    install_requires=[],
    extras_require={"test": ["pytest", "pytest-mock"], "examples": ["bokeh"]},
)
